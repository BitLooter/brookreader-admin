/*!
Macro implementation for doccommentreader. See its documentation for details.
!*/

use std::collections::BTreeMap;

use proc_macro::TokenStream;
use quote::quote;
use syn::{self, Attribute, Data, DeriveInput, Fields, Lit, Meta};


/// Main macro code
#[proc_macro_derive(DocCommentReader, attributes(cfb_default, cfb_required))]
pub fn doccommentreader_derive(input: TokenStream) -> TokenStream {
    let ast: DeriveInput = syn::parse(input)
        .expect("DocCommentReader could not parse input");
    let type_docs = extract_item_docs(&ast.attrs);
    let mut fields_docs = BTreeMap::<String, String>::new();

    if let Data::Struct(data_struct) = &ast.data {
        if let Fields::Named(named_fields) = &data_struct.fields {
            for field in named_fields.named.iter() {
                let field_name = format!("{}", field.ident.as_ref()
                    .expect("Could not get field ident")
                );
                let field_doc = extract_item_docs(&field.attrs);
                fields_docs.insert(field_name, field_doc);
            }
        } else {
            panic!("Can only derive DocCommentReader on structs with named fields");
        }
    } else {
        panic!("Can only derive DocCommentReader on structs");
    }

    impl_doccommentreader(&ast, type_docs, fields_docs)
}

/// Extracts the doc comments from a node's attributes
fn extract_item_docs(attrs: &Vec<Attribute>) -> String {
    let mut docs = String::new();

    for attr in attrs.iter() {
        let meta = attr.parse_meta()
            .expect("Could not parse field attributes");
        if let Meta::NameValue(meta_named) = meta {
            let attr_name = &meta_named.path.segments.first()
                .expect("Named attributes have a first segment in the path")
                .ident;
            // Doc comments are converted by the compiler into named attributes
            // called "doc"
            if attr_name == "doc" {
                if let Lit::Str(meta_string) = meta_named.lit {
                    // Trim initial space before pushing string, formatting
                    // after first space preserved
                    let input_string = meta_string.value();
                    let trimmed_string = if input_string.starts_with(' ') {
                        input_string[1..].trim_end()
                    } else {
                        input_string.trim()
                    };
                    docs.push_str(trimmed_string);
                    docs.push('\n');
                }
            }
        }
    }

    docs.trim().to_owned()
}

/// Generate the implementation of the ConfigFileBuilder trait
fn impl_doccommentreader(
    ast: &DeriveInput,
    struct_docs: String,
    fields_docs: BTreeMap<String, String>
) -> TokenStream {
    let name = &ast.ident;

    // Construct Iterators for use in quote! block. Note that because field_docs
    // is a BTreeMap keys/values will be ordered.
    let field_names = fields_docs.keys();
    let field_comments = fields_docs.values();

    let gen = quote! {
        impl DocCommentReader for #name {
            fn get_doc_comments() -> std::collections::HashMap<&'static str, &'static str> {
                let mut field_doc_comments = HashMap::<&str, &str>::new();
                #(
                    field_doc_comments.insert(#field_names, #field_comments);
                )*
                field_doc_comments
            }

            fn get_type_doc_comment() -> &'static str {
                #struct_docs
            }
        }
    };
    gen.into()
}
