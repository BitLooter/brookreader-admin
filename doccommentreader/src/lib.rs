/*!
Implements a derive macro that adds a get_doc_comments() method on a struct
that returns a HashMap containing the field names as keys and the documentation
comments as values. Also implements a get_type_doc_comment() method that
returns the doc comment for the type itself.
!*/

pub use doccommentreader_derive::DocCommentReader;

pub trait DocCommentReader {
    /// Returns a hashmap where the key is a &str containing the name of the
    /// field on the struct and the value is a &str with the documentation
    /// comments for the field. Multi-line comments are merged into a single
    /// string joined by \n. An initial space is removed from the comment, but
    /// formatting is otherwise preserved. Fields without comments have an
    /// empty string.
    fn get_doc_comments() -> std::collections::HashMap<&'static str, &'static str>;
    /// Similar to get_doc_comments, but for the documentation comment on the
    /// type itself. Same formatting rules apply.
    fn get_type_doc_comment() -> &'static str;
}
