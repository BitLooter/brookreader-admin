use chrono::{Utc, TimeZone};
use clap::ArgMatches;
use insta::assert_yaml_snapshot;

use brookreader_admin::Config;
use brookreader_admin::mediadb;
use brookreader_admin::metadata::BookMetadata;
use brookreader_admin::scan;

mod common;


#[test]
fn read_from_media_database() {
    let args = ArgMatches::new();
    let config_path = common::testenv_root().join("Brookreader.toml");
    // TODO: Path argument should be generic
    // TODO: Args should be merged in separate call
    // TODO: Temp file for database
    let config = Config::load(&config_path, &args)
        .expect(&format!("Could not load test environment configuration file {:?}", &config_path));
    // TODO: Scan to temp file for test
    scan::scan(&config)
        .expect("Could not scan test environment");
    let db = mediadb::MediaDatabase::open(&config.database_location)
        .expect("Could not open test instance database");

    // Get list of all book IDs
    let mut book_ids = db.get_all_book_ids().collect::<Vec<_>>();
    book_ids.sort_unstable();
    assert_yaml_snapshot!("get_book_ids", book_ids);

    // Get all books in database
    let mut books = db.get_all_books().collect::<Vec<_>>();
    books.sort_unstable_by(|b1, b2| b1.id.partial_cmp(&b2.id).unwrap());
    assert_yaml_snapshot!("get_books", books);

    // Get a book by its ID
    let book_with_id = db.get_book("test-cbz")
        .expect("Book with ID test-cbz not found");
    assert_eq!("test-cbz", book_with_id.title);
    assert!(db.get_book("not_a_valid_book_id").is_none());

    // Get books added after a given date
    let test_date = Utc.ymd(2020, 1, 1).and_hms(0, 0, 0);
    let books_after_date = db.get_all_books_added_after(test_date)
        .collect::<Vec<_>>();
    assert_eq!(1, books_after_date.len());
    assert_eq!("test-dir", books_after_date[0].id);

    // Get list of all shelf IDs
    // TODO: Main shelf should be named something like %main/$main? Maybe have a modifier for
    // special/generated shelves not allowed in other IDs?
    let mut shelf_ids = db.get_all_shelf_ids().collect::<Vec<_>>();
    shelf_ids.sort_unstable();
    assert_yaml_snapshot!("get_shelf_ids", shelf_ids);

    // Get all shelves in database
    let mut shelves = db.get_all_shelves().collect::<Vec<_>>();
    shelves.sort_unstable_by(|b1, b2| b1.id.partial_cmp(&b2.id).unwrap());
    assert_yaml_snapshot!("get_shelves", shelves);

    // Get a shelf by its ID
    let shelf_with_id = db.get_shelf("book_formats")
        .expect("Shelf with ID book_formats not found");
    assert_eq!("book_formats", shelf_with_id.name);
    assert!(db.get_shelf("not_a_valid_shelf_id").is_none());

    // Retrieving by path
    assert!(db.get_book_with_path("book_formats/test-cbz.cbz").is_some());
    assert!(db.get_book_with_path("path/does/not/exist.cbz").is_none());
    assert!(db.get_shelf_with_path("test_cases/nested").is_some());
    assert!(db.get_shelf_with_path("path/does/not/exist").is_none());

    // Database information
    assert_eq!(16, db.num_books());
}

// TODO: Database write methods
#[test]
fn write_to_media_database() {
    // TODO:DATABASE: Does db still need to be mutable after database implemented?
    let mut db = mediadb::MediaDatabase::new("filename", "filename");

    assert_eq!(0, db.info.database_version);

    let new_book_id = "new_id";
    let new_book_title = "New book";
    let meta = BookMetadata {
        title: new_book_title.into(),
        date_added: Utc::now()
    };
    let mut book = mediadb::Book::new(
        new_book_id,
        meta,
        "new_url.cbz",
        "path/to/book.cbz"
    );

    assert_eq!(0, db.num_books());

    // Adding a new book
    db.add_or_update_book(book.clone())
        .expect("Could not add book to database");
    let db_book = db.get_book(new_book_id)
        .expect("Could not get book from database");
    assert_eq!(new_book_title, db_book.title);

    // Updating an existing book
    assert_eq!(1, db.num_books());
    book.title = "Updated title".to_owned();
    db.add_or_update_book(book.clone())
        .expect("Could not update existing book");
    let updated_db_book = db.get_book(new_book_id)
        .expect("Could not get updated book from database");
    assert_eq!("Updated title", updated_db_book.title);
    assert_eq!(1, db.num_books());

    // Deleting a book
    book.id = "delete_book".to_owned();
    db.add_or_update_book(book)
        .expect("Could not add book to delete");
    assert_eq!(2, db.num_books());
    db.delete_book("delete_book")
        .expect("Could not delete book");
    assert_eq!(1, db.num_books());

    // Creating shelves
    assert_eq!(0, db.num_shelves());
    db.create_shelf("New shelf", "new_shelf", "/path/of/shelf/", None)
        .expect("Could not create shelf");
    db.create_shelf("Child shelf", "child_shelf", "/path/of/shelf/child/", Some("new_shelf"))
        .expect("Could not create child shelf");
    db.create_shelf("Orphan shelf", "orphan_shelf", "/path/of/shelf/orphan/", None)
        .expect("Could not create child shelf");
    assert_eq!(3, db.num_shelves());
    let shelf = db.get_shelf("new_shelf")
        .expect("Newly created shelf was not found");
    assert_eq!(vec!["child_shelf"], shelf.subshelves);

    // Updating shelf contents
    assert_eq!(Vec::<String>::new(), shelf.books);
    db.set_shelf_contents("new_shelf", vec![new_book_id])
        .expect("Could not set shelf contents");
    db.set_shelf_subshelves("new_shelf", vec!["child_shelf", "orphan_shelf"])
        .expect("Could not set shelf subshelves");
    let updated_shelf = db.get_shelf("new_shelf")
        .expect("Newly created shelf was not found");
    assert_eq!(vec![new_book_id], updated_shelf.books);
    assert_eq!(vec!["child_shelf", "orphan_shelf"], updated_shelf.subshelves);

    // Deleting a shelf
    assert_eq!(3, db.num_shelves());
    db.delete_shelf("orphan_shelf")
        .expect("Could not delete shelf");
    assert_eq!(2, db.num_shelves());
}
