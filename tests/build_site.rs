mod common;

use std::{fs::File, path::Path};

use brookreader_admin::build;

// Removing these lines cause errors in common/mod.rs
use brookreader_admin::Config;
use brookreader_admin::scan;


/// Test building a full site from input files
#[test]
fn build_test_site_output() {
    // NOTE: Test is currently very slow, should benchmark and find the slowdown eventually

    let mut config = common::testenv_environment_populated();
    let out_path = tempfile::tempdir()
        .expect("Could not create temporary build directory");
    config.build_output_path = out_path.path().to_owned();
    // TODO: Refactor brookreader_data_path. Rename to build_data_root? Allow relative paths?
    // Maybe rethink if build_data_path is needed? Utility methods on config to get normalized
    // paths? Basically we shouldn't have to set a bunch of paths manually when the build root
    // changes.
    config.brookreader_data_path = out_path.path().join("data");
    config.build_cover_path = config.brookreader_data_path.join("covers");

    // build_data() tests
    //////////////////////

    build::build_data(&config)
        .expect("Could not build site data");

    // Valid bookshelf file was written
    let bookshelf = parse_json(&config.brookreader_data_path.join("bookshelf.json"));
    assert_eq!("Home", &bookshelf["info"]["name"]);

    // Cover images were generated
    assert!(config.build_cover_path.join("test-cbz-full.jpg").exists());
    assert!(config.build_cover_path.join("test-cbz-thumb.png").exists());
    assert!(config.build_cover_path.join("shelves/book_formats-thumb.png").exists());

    // Showcase bookshelf was generated and valid JSON
    let showcase = parse_json(&config.brookreader_data_path.join("showcase.json"));
    assert_eq!("Showcase", &showcase["info"]["name"]);

    // Stats were generated
    // TODO: Should stats be part of the bookshelf file?
    let stats = parse_json(&config.brookreader_data_path.join("stats.json"));
    assert_eq!("0.6.0", &stats["server"]["version"]);

    // Outdated/unknown files purged from output
    let purgable_files = vec![
        // TODO: Purge details
        // config.brookreader_data_path.join("details/nonexistant_book.json"),
        config.build_cover_path.join("nonexistant_book-full.jpg"),
        config.build_cover_path.join("nonexistant_book-thumb.png"),
    ];
    for purgable in purgable_files.iter() {
        File::create(purgable)
            .expect(&format!("Could not create purgable file {:?}", purgable));
        assert!(purgable.exists());
    }
    // This rebuild should execute quickly and remove the above files
    build::build_data(&config)
        .expect("Could not build site data");
    for purged in purgable_files.iter() {
        assert!(!purged.exists(), "File {:?} was not removed", purged);
    }

    // build_fe() tests
    ////////////////////

    let removable_file = config.build_output_path.join("remove.me");
    File::create(&removable_file)
        .expect(&format!("Could not create file to be removed {:?}", &removable_file));
    assert!(removable_file.exists());

    build::build_fe(&config)
        .expect("Could not build frontend files");

    // Output directory was cleaned
    assert!(!removable_file.exists(), "Unclean file {:?} still exists", &removable_file);

    // Frontend files were built
    assert!(config.build_output_path.join("FRONTEND").exists());

    // Content files were linked
    let content_link = config.build_output_path.join("books");
    assert!(
        content_link.symlink_metadata()
            .expect(&format!("Could not get metadata for {:?}", &content_link))
            .file_type()
            .is_symlink()
    );
}

// Read and parse a JSON file. Panics on error.
fn parse_json(json_path: &Path) -> serde_json::Value {
    assert!(json_path.exists());
    let json_reader = File::open(&json_path)
        .expect(&format!("Could not open JSON file {:?}", &json_path));
    let json_value = serde_json::from_reader(json_reader)
        .expect(&format!("Could not parse bookshelf file {:?}", &json_path));
    json_value
}
