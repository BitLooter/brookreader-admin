// Rust incorrectly marks several functions in this file as dead_code, even though they're
// used by tests. This might be because it's imported in lib using a path attribute
// to allow access to unit tests. This could be a bug in Rust/Cargo, look into this later.
#![allow(dead_code)]


use std::path::PathBuf;
use clap::ArgMatches;

use crate::Config;
use crate::scan;


pub const TOTAL_BOOKS: usize = 16;

/// Root path of the Brookreader test instance
pub fn testenv_root() -> PathBuf {
    PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("tests/library")
}

/// Path of a book sample file by name
pub fn testenv_get_book_path(book_name: &str) -> PathBuf {
    // TODO: Can we make these values const?
    let books_root = testenv_root().join("static/books/");
    let path = match book_name {
        "test-cbz" => books_root.join("book_formats/test-cbz.cbz"),
        "test-store" => books_root.join("test_cases/test-store.cbz"),
        _ => panic!("Unknown book {}", book_name)
    };

    path
}

/// Configuration of the test instance
pub fn testenv_config() -> Config {
    let args = ArgMatches::new();
    let root_path = testenv_root();
    let config_path = root_path.join("Brookreader.toml");
    Config::load(&config_path, &args)
        .expect(&format!("Could not load configuration from {:?}", &config_path))
}


/// Execution environment of the test instance, with the database populated
pub fn testenv_environment_populated() -> Config {
    let config = testenv_config();
    scan::scan(&config)
        .expect("Could not scan test environment");
    config
}
