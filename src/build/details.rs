use std::{
    fs::File,
    path::{Path, PathBuf}};
use chrono::{DateTime, Utc};
use serde::Serialize;
use snafu::{ResultExt, Snafu};

use crate::mediadb::Book;


#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Could not serialize details for book {} to JSON: {}", id, source))]
    JsonSerializeError { id: String, source: serde_json::error::Error },
    #[snafu(display("Could not write details JSON for book {} to {}: {}", id, path.display(), source))]
    JsonWriteError { id: String, path: PathBuf, source: std::io::Error },
}
type Result<T, E = Error> = std::result::Result<T, E>;

/// Used for serializing a Book struct into a details JSON for the frontend
#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct BookDetails<'book> {
    // TODO: Document fields
    pub id: &'book str,
    pub title: &'book str,
    pub date_added: &'book DateTime<Utc>,
    /// Full URL of book file. Database URL is relative and (absolute) URL for details
    /// JSON generated and owned by the struct.
    pub url: String,
    // TODO: Why is this owned? Fix if not needed, or document why
    #[serde(skip_serializing_if = "Option::is_none")]
    pub page_urls: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prev_id: &'book Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub next_id: &'book Option<String>,
}

impl<'book> BookDetails<'book> {
    // TODO: Consider implementing From<&Book> instead
    /// Creates a BookDetails out of a Book. Book's (relative) URL is appended
    /// to base_url on struct creation.
    pub fn from_book(book: &'book Book, base_url: &str) -> Self {
        let page_urls = book.page_urls.as_ref().map(|v|
            v.iter().map(|u| format!("{}/{}", base_url, &u)).collect()
        );
        Self {
            id: &book.id,
            title: &book.title,
            date_added: &book.date_added,
            // TODO: Use a library to ensure URLs are formatted correctly?
            url: format!("{}/{}", base_url, &book.url),
            page_urls,
            prev_id: &book.prev_id,
            next_id: &book.next_id,
        }
    }

    /// Write JSON containing full metadata on this specific book
    pub fn write_json(&self, path: &Path) -> Result<()> {
        // let book_full = BookFullSerialize::from(self);
        let json_file = File::create(path)
            .context(JsonWriteError {id: self.id, path})?;
        serde_json::to_writer(&json_file, self)
            .context(JsonSerializeError {id: self.id})?;
        Ok(())
    }
}


#[cfg(test)]
#[path = "./details.test.rs"]
mod details_test;
