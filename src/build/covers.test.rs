use chrono::Utc;

use crate::tests_common;
use crate::mediadb::Book;
use crate::metadata::BookMetadata;


// TODO: Cover options when implemented

#[test]
fn generate_book_cover() {
    let temp_cover_path = tempfile::tempdir()
        .expect("Could not create temporary directory");
    let temp_cover_path = temp_cover_path.path().to_owned();

    let mut config = tests_common::testenv_config();
    config.build_cover_path = temp_cover_path;
    let manager = super::CoverManager::new(&config);

    let test_metadata = BookMetadata {
        title: "Test book".into(),
        date_added: Utc::now()
    };
    let test_book_path = tests_common::testenv_get_book_path("test-cbz");
    let test_book = Book::new("testbook", test_metadata, "no.url", &test_book_path);
    manager.ensure_book_cover(&test_book)
        .expect("Could not generate book cover");

    // These filenames will need to be updated when cover format options are improved
    assert!(config.build_cover_path.join("testbook-full.jpg").exists());
    assert!(config.build_cover_path.join("testbook-thumb.png").exists());
}

// TODO: Test disabled until it can have access to the database. May need to wait until
// ExecutionEnvironment is implemented, or lazy-loaded test DB generation.
/*#[test]
fn generate_shelf_cover() {
    let temp_cover_path = tempfile::tempdir()
        .expect("Could not create temporary directory");
    let temp_cover_path = temp_cover_path.path().to_owned();

    let mut config = tests_common::testenv_config();
    config.build_cover_path = temp_cover_path;
    let manager = super::CoverManager::new(&config);

    // TODO: Implement testenv_get_book to simplify this
    let test_metadata = BookMetadata {
        title: "Test book".into(),
        date_added: Utc::now()
    };
    let test_cbr_path = tests_common::testenv_get_book_path("test-cbz");
    let test_cbr = Book::new("testbook-cbr", test_metadata, "no.url", &test_cbr_path);
    let test_cbz_path = tests_common::testenv_get_book_path("test-cbz");
    let test_cbz = Book::new("testbook-cbz", test_metadata, "no.url", &test_cbz_path);
    let test_shelf_path = testenv_root().join("static/books/book_formats");
    let mut test_shelf = Shelf::new("test-shelf", "Test shelf", test_shelf_path);
    test_shelf.books.push(test_cbr.id.clone());
    test_shelf.books.push(test_cbz.id.clone());

    for e in config.build_cover_path.read_dir().unwrap() {
        dbg!(e);
    }
    manager.ensure_shelf_cover(&test_shelf, &db)
        .expect("Could not generate shelf cover");

    // These filenames will need to be updated when cover format options are improved
    assert!(config.build_cover_path.join("testbook-full.jpg").exists());
    assert!(config.build_cover_path.join("testbook-thumb.png").exists());
}*/
