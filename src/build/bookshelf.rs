use std::{
    collections::HashMap,
    fs,
    path::{Path, PathBuf}
};
use serde::Serialize;
use snafu::{OptionExt, ResultExt, Snafu};

use crate::mediadb::{self, Book, Shelf};


#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Could not serialize bookshelf to JSON: {}", source))]
    BookshelfJsonSerialize { source: serde_json::error::Error },
    #[snafu(display("Could not write bookshelf JSON to {}: {}", path.display(), source))]
    BookshelfJsonWrite { path: PathBuf, source: std::io::Error },
    #[snafu(display("Duplicate book ID {}", id))]
    BookDuplicate { id: String },
    #[snafu(display("Could not serialize metadata for {} to JSON: {}", id, source))]
    MetadataJsonSerialize { id: String, source: serde_json::error::Error },
    #[snafu(display("Could not write metadata JSON for {} to {}: {}", id, path.display(), source))]
    MetadataJsonWrite { id: String, path: PathBuf, source: std::io::Error },
    #[snafu(display("Shelf with ID {} not found in bookshelf", id))]
    ShelfNotFound { id: String },
}
type Result<T, E = Error> = std::result::Result<T, E>;

/** Main book metadata index

Contains all the information Brookreader needs to display the catalog. Not all
book information is contained in this, full information is stored in a
BookDetails and is written to a unique file for each book to load when needed.
*/
#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Bookshelf {
    pub info: BookshelfInfo,
    pub shelves: HashMap<String, BookshelfShelf>,
    pub books: HashMap<String, BookshelfBook>,
}

impl Bookshelf {
    /// Construct a new Bookshelf
    pub fn new<S: Into<String>>(name: S) -> Self {
      Self {
        info: BookshelfInfo {
          name: name.into()
        },
        shelves: HashMap::new(),
        books: HashMap::new()
      }
    }

    /// Construct a new Bookshelf with a root (main) shelf already present
    pub fn new_with_main<SName, SRoot>(name: SName, root_shelf_name: SRoot) -> Self
    where SName: Into<String>, SRoot: Into<String> {
        let mut bookshelf = Self::new(name);

        bookshelf.shelves.insert(
            mediadb::MAIN_SHELF_ID.to_string(),
            BookshelfShelf::new(root_shelf_name.into())
        );

        bookshelf
    }

    /// Create and add a shelf to the bookshelf
    pub fn add_new_shelf<SId, SName, SParent>(&mut self, id: SId, name: SName, parent_id: SParent) -> Result<()>
    where SId: Into<String>, SName: Into<String>, SParent: AsRef<str> {
        let shelf_id = id.into();
        let parent_id = parent_id.as_ref();
        self.shelves.get_mut(parent_id)
            .context(ShelfNotFound {id: parent_id})?
            .subshelves.push(shelf_id.clone());
        self.shelves.insert(shelf_id, BookshelfShelf::new(name));

        Ok(())
    }

    // TODO: Consider renameing to something like import_book() to indicate it's intended
    //  to take books directly from the database
    /// Move a database book into the the bookshelf
    pub fn push_book(&mut self, book: Book) {
        self.books.insert(book.id.clone(), BookshelfBook::from(book));
    }

    /// Move a database book into the the bookshelf and add its ID to a shelf.
    /// Returns an error if shelf_id not in shelves.
    pub fn push_book_to_shelf<S: AsRef<str>>(&mut self, book: Book, shelf_id: S) -> Result<()> {
        let shelf_id = shelf_id.as_ref();
        self.shelves.get_mut(shelf_id)
            .context(ShelfNotFound {id: shelf_id})?
            .books.push(book.id.clone());
        self.push_book(book);

        Ok(())
    }

    /// Move a database shelf into the bookshelf
    pub fn push_shelf(&mut self, shelf: Shelf) {
        let shelf_id = shelf.id.clone();
        self.shelves.insert(shelf_id, BookshelfShelf::from(shelf));
    }

    /// Write out bookshelf as JSON to path
    pub fn write_json(&self, bookshelf_path: &Path) -> Result<()> {
        let json_file = fs::File::create(bookshelf_path)
            .context(BookshelfJsonWrite {path: bookshelf_path})?;
        serde_json::to_writer(&json_file, self)
            .context(BookshelfJsonSerialize {})?;
        Ok(())
    }
}

/// Header info for a bookshelf file
#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct BookshelfInfo {
    pub name: String
}

/// Books are grouped into shelves, struct contains shelf metadata and book list
#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct BookshelfShelf {
    pub name: String,
    pub subshelves: Vec<String>,
    pub books: Vec<String>,
}

impl BookshelfShelf {
    pub fn new<S: Into<String>>(name: S) -> Self {
        Self {
            books: Vec::new(),
            name: name.into(),
            subshelves: Vec::new(),
        }
    }
}

impl From<Shelf> for BookshelfShelf {
    /// Converts a database shelf into a bookshelf shelf
    fn from(db_shelf: Shelf) -> Self {
        Self {
            name: db_shelf.name,
            subshelves: db_shelf.subshelves,
            books: db_shelf.books,
        }
    }
}

/// Most fields are skipped by default - information that can be found
/// elsewhere (like the book ID) or can be loaded on demand without significant
/// UX impact (like a long description) are omitted from the main bookshelf
/// file, to reduce load times of the index. This information will be found in
/// individual book metadata files, that are loaded on demand. The
/// BookFullSerialize struct is used to serialize these fields.
#[derive(Clone, Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct BookshelfBook {
    pub title: String,
}

impl From<Book> for BookshelfBook {
    /// Converts a database book into a bookshelf book. Only keeps information
    /// relevant to the bookshelf, use BookshelfBookDetails when outputting
    /// full book metadata.
    fn from(db_book: Book) -> Self {
        Self {
            title: db_book.title,
        }
    }
}


#[cfg(test)]
#[path = "./bookshelf.test.rs"]
mod bookshelf_test;
