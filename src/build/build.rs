use std::{fs, io, path::{Path, PathBuf}};
use snafu::{OptionExt, ResultExt, Snafu, ensure};

use crate::config::Config;
use crate::mediadb::{self, MediaDatabase};
use crate::showcase::ShowcaseList;
use crate::ui;
use crate::util::{self, copy_dir_contents, dir_paths_iter, link_dir_contents};
use super::bookshelf::{self, Bookshelf};
use super::covers;
use super::details::{self, BookDetails};
use super::stats::{self, Stats};


const BOOKSHELF_FILENAME: &str = "bookshelf.json";
const DETAILS_DIR: &str = "details";
const FEATURED_SHELF_ID: &str = "featured";
const RECENT_SHELF_ID: &str = "recent";
const SHOWCASE_FILENAME: &str = "showcase.json";
const STATS_FILENAME: &str = "stats.json";

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Problem with bookshelf '{}': {}", bookshelf_name, source))]
    BookshelfOperationError { bookshelf_name: String, source: bookshelf::Error },
    #[snafu(display("Unknown Book ID: {}", id))]
    BookUnknownError { id: String },
    #[snafu(display("Could not open database: {}", source))]
    DatabaseLoadError { source: mediadb::Error },
    #[snafu(display("Item '{}' in {} overwrites build data directory", name.display(), source_path.display()))]
    DataDirOverwriteError { name: PathBuf, source_path: PathBuf },
    #[snafu(display("Could not write media details: {}", source))]
    DetailsError { source: details::Error },
    #[snafu(display("Could not copy frontend files: {}", source))]
    FrontendCopyError { source: util::Error },
    #[snafu(display("Could not create path {}: {}", path.display(), source))]
    PathCreateError { path: PathBuf, source: io::Error },
    #[snafu(display("Path not found: {}", path.display()))]
    PathMissingError { path: PathBuf },
    #[snafu(display("Could not read from path {}: {}", path.display(), source))]
    PathReadError { path: PathBuf, source: io::Error },
    #[snafu(display("Unable to remove path {}: {}", path.display(), source))]
    PathRemoveError { path: PathBuf, source: io::Error },
    #[snafu(display("Unknown Shelf ID: {}", id))]
    ShelfUnknownError { id: String },
    #[snafu(display("Could not generate stats: {}", source))]
    StatsGenerateError { source: stats::Error },
    #[snafu(display("Could not write stats: {}", source))]
    StatsWriteError { source: stats::Error },
    #[snafu(display("Ui error: {}", source))]
    UiError { source: ui::Error },
}

impl From<ui::Error> for Error {
    fn from(source: ui::Error) -> Self {
        Self::UiError {source}
    }
}

type Result<T, E = Error> = std::result::Result<T, E>;

/// Build site data (index, covers, etc.) from database
pub fn build_data(config: &Config) -> Result<()> {
    let database = MediaDatabase::open(&config.database_location)
        .context(DatabaseLoadError {})?;
    config.ui_hooks.build_data_begin()?;

    create_directories(config)?;
    write_index(&database, config)?;
    extract_covers(&database, config)?;
    generate_showcase(&database, config)?;
    generate_stats(&database, config)?;
    purge_outdated_files(&database, config)?;

    config.ui_hooks.build_data_end()?;
    Ok(())
}

/// Build site frontend from source directories
pub fn build_fe(config: &Config) -> Result<()> {
    config.ui_hooks.build_fe_begin()?;

    // Remove everything from the build directory except the data dir
    clean_dir(&config.build_output_path, vec![
        &config.brookreader_data_path,
    ])?;

    // Closure does validity checks on a path before copying its contents
    let check_and_copy_source_dir = |source_path: &Path, link_contents: bool| -> Result<()> {
        // Check that source directory exists
        ensure!(
            source_path.exists(),
            PathMissingError {path: source_path}
        );
        // Check that nothing in source directory overwrites data dir
        let build_data_name = config.brookreader_data_path
            .strip_prefix(&config.build_output_path)
            .expect("Data dir path validity checked when config was loaded");
        ensure!(
            !source_path.join(build_data_name).exists(),
            DataDirOverwriteError {name: &build_data_name, source_path}
        );

        if link_contents {
            link_dir_contents(source_path, &config.build_output_path)
                .context(FrontendCopyError {})?;
        } else {
            copy_dir_contents(source_path, &config.build_output_path)
                .context(FrontendCopyError {})?;
        }

        Ok(())
    };

    if let Some(fe_path) = config.build_frontend_path.as_ref() {
        check_and_copy_source_dir(fe_path, false)?;
    }
    if let Some(static_path) = config.build_static_path.as_ref() {
        check_and_copy_source_dir(static_path, config.build_static_symlink)?;
    }

    config.ui_hooks.build_fe_end()?;
    Ok(())
}

/// Create output directory structure
fn create_directories(config: &Config) -> Result<()> {
    let details_base_path = &config.brookreader_data_path.join(DETAILS_DIR);
    std::fs::create_dir_all(&details_base_path)
        .context( PathCreateError {path: details_base_path} )?;
    std::fs::create_dir_all(config.build_cover_path.join("shelves"))
        .context( PathCreateError {path: details_base_path} )?;
    Ok(())
}

/// Write bookshelf and other related index files
fn write_index(database: &MediaDatabase, config: &Config) -> Result<()> {
    let details_base_path = &config.brookreader_data_path.join(DETAILS_DIR);
    let mut bookshelf = Bookshelf::new("Home");

    for shelf in database.get_all_shelves() {
        bookshelf.push_shelf(shelf);
    }
    for book in database.get_all_books() {
        let book_details = BookDetails::from_book(&book, &config.media_books_url);
        let book_details_path = details_base_path.join(format!("{}.json", &book.id));
        book_details.write_json(&book_details_path)
            .context( DetailsError {} )?;
        bookshelf.push_book(book);
    }

    bookshelf.write_json(&config.brookreader_data_path.join(BOOKSHELF_FILENAME))
        .context( BookshelfOperationError {bookshelf_name: &bookshelf.info.name} )?;

    Ok(())
}

/// Extract or generate cover images for media and shelves
fn extract_covers(database: &MediaDatabase, config: &Config) -> Result<()> {
    let cover_manager = covers::CoverManager::new(config);
    config.ui_hooks.build_data_covers_begin()?;

    // Covers for media
    for book in database.get_all_books() {
        config.ui_hooks.build_data_cover_media(&book)?;
        cover_manager.ensure_book_cover(&book)
            .or_else(|e| config.ui_hooks.build_data_cover_media_error(&book, &format!("{}", e)) )?;
    }

    // Covers for shelves
    for shelf in database.get_all_shelves()
        .filter(|s| s.id != mediadb::MAIN_SHELF_ID)
    {
        config.ui_hooks.build_data_cover_shelf(&shelf)?;
        cover_manager.ensure_shelf_cover(&shelf, &database)
            .or_else(|e| config.ui_hooks.build_data_cover_shelf_error(&shelf, &format!("{}", e)) )?;
    }

    config.ui_hooks.build_data_covers_end()?;
    Ok(())
}

/// Build the showcase bookcase from showcase list files
fn generate_showcase(database: &MediaDatabase, config: &Config) -> Result<()> {
    let mut showcase = Bookshelf::new_with_main("Showcase", "Main (unused)");

    // Featured
    if config.showcase_sections.iter().any(|s| s == FEATURED_SHELF_ID) {
        let featured_list_path = config.base_path.join("featured.txt");
        ensure!(featured_list_path.exists(), PathMissingError {path: featured_list_path});
        let featured_list = ShowcaseList::load(&featured_list_path)
            .context(PathReadError {path: &featured_list_path})?;
        showcase.add_new_shelf(FEATURED_SHELF_ID, "Featured items", mediadb::MAIN_SHELF_ID)
            .expect("MAIN_SHELF_ID should be in bookshelf");

        for book_id in featured_list.items {
            let book = database.get_book(&book_id)
                .context(BookUnknownError {id: &book_id})?;
            showcase.push_book_to_shelf(book, FEATURED_SHELF_ID)
                .expect("Could not add featured book to showcase");
        }

        for shelf_id in featured_list.shelves {
            let shelf = database.get_shelf(&shelf_id)
                .context(ShelfUnknownError {id: &shelf_id})?;
            recursive_shelf_import(database, &mut showcase, &shelf, FEATURED_SHELF_ID)?;
        }
    }

    // Recent
    if config.showcase_sections.contains(&"recent".to_owned()) {
        showcase.add_new_shelf(RECENT_SHELF_ID, "Recently added", mediadb::MAIN_SHELF_ID)
            .expect("MAIN_SHELF_ID should be in bookshelf");

        for book in database.get_all_books_added_after(config.showcase_recent_threshold) {
            showcase.push_book_to_shelf(book, RECENT_SHELF_ID)
                .context(BookshelfOperationError {bookshelf_name: &showcase.info.name})?;
        }
    }

    showcase.write_json(&config.brookreader_data_path.join(SHOWCASE_FILENAME))
        .context( BookshelfOperationError {bookshelf_name: "Showcase"} )?;

    Ok(())
}

/// Build stats file
fn generate_stats(database: &MediaDatabase, config: &Config) -> Result<()> {
    let stats = Stats::new(&database, &config.media_books_root)
        .context( StatsGenerateError {} )?;
    config.ui_hooks.build_data_stats(&stats)?;
    stats.write(&config.brookreader_data_path.join(STATS_FILENAME))
        .context( StatsWriteError {} )?;

    Ok(())
}

/// Remove outdated files from data directory
fn purge_outdated_files(database: &MediaDatabase, config: &Config) -> Result<()> {
    let valid_cover_stems = database.get_all_book_ids()
        .map(|i| {
            vec![
                std::ffi::OsString::from(format!("{}-full", i)),
                std::ffi::OsString::from(format!("{}-thumb", i))
            ]
        })
        .flatten()
        .collect::<std::collections::HashSet<_>>();
    for entry in fs::read_dir(&config.build_cover_path)
        .context(PathReadError {path: &config.build_cover_path})?
    {
        let file_path = entry.context(PathReadError {path: &config.build_cover_path})?.path();
        if file_path.is_dir() {
            continue
        }
        let file_stem = file_path.file_stem()
            .expect("All entries should have a file name");
        if !valid_cover_stems.contains(file_stem) {
            fs::remove_file(&file_path)
                .context(PathRemoveError {path: file_path})?;
        }
    }

    let valid_shelf_cover_stems = database.get_all_shelf_ids()
        .map(|i| {
            vec![
                std::ffi::OsString::from(format!("{}-full", i)),
                std::ffi::OsString::from(format!("{}-thumb", i))
            ]
        })
        .flatten()
        .collect::<std::collections::HashSet<_>>();
    let cover_shelf_dir = config.build_cover_path.join("shelves");
    for entry in fs::read_dir(&cover_shelf_dir)
        .context(PathReadError {path: &cover_shelf_dir})?
    {
        let file_path = entry.context(PathReadError {path: &cover_shelf_dir})?.path();
        if file_path.is_dir() {
            continue
        }
        let file_stem = file_path.file_stem()
            .expect("All entries should have a file name");
        if !valid_shelf_cover_stems.contains(file_stem) {
            fs::remove_file(&file_path)
                .context(PathRemoveError {path: file_path})?;
        }
    }

    Ok(())
}

/// Remove all files from a directory except for those in keep_files
fn clean_dir(dir: &Path, keep_files: Vec<&Path>) -> Result<()> {
    let remove_iter = dir_paths_iter(dir)
        .context(PathReadError {path: dir})?
        .filter(|f| !keep_files.contains(&f.as_path()) );

    for entry in remove_iter {
        if entry.is_dir() {
            fs::remove_dir_all(&entry)
                .context(PathRemoveError {path: &entry})?;
        } else {
            fs::remove_file(&entry)
                .context(PathRemoveError {path: &entry})?;
        }
    }

    Ok(())
}

// TODO: Consider making this part of Bookshelf
/// Import information from database about shelf, subshelves, and all associated books
fn recursive_shelf_import(
    database: &MediaDatabase, bookshelf: &mut Bookshelf, shelf: &mediadb::Shelf, parent_id: &str
) -> Result<()> {
    bookshelf.add_new_shelf(&shelf.id, &shelf.name, parent_id)
        .expect("Parent ID should have been added before calling this function");

    // Import information for all books on this shelf
    for shelf_book_id in shelf.books.iter() {
        let book = database.get_book(shelf_book_id)
            .context(BookUnknownError {id: shelf_book_id})?;
        bookshelf.push_book_to_shelf(book, &shelf.id)
            .expect("Shelf ID was just added");
    }

    for subshelf_id in shelf.subshelves.iter() {
        let subshelf = database.get_shelf(&subshelf_id)
            .context(ShelfUnknownError {id: subshelf_id})?;
        recursive_shelf_import(database, bookshelf, &subshelf, &shelf.id)?;
    }

    Ok(())
}


// TODO: Not well suited to unit testing, implement integration tests for this
