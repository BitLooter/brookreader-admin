use chrono::Utc;
use tempfile::NamedTempFile;

use crate::mediadb::Book;
use crate::metadata::BookMetadata;


#[test]
fn details_smoke_test() {
    // TODO: Maybe make BookMetadata::new if only to simplify tests
    let test_metadata = BookMetadata {
        title: "Test book".into(),
        date_added: Utc::now()
    };
    let test_book = Book::new("testbook", test_metadata, "no_url.cbz", "/no/path");
    let test_details = super::BookDetails::from_book(&test_book, "http://example.com");

    // Note the trailing slash missing from base_url above - BookDetails should properly
    // format URLs
    assert_eq!("http://example.com/no_url.cbz", &test_details.url);

    let temp_file = NamedTempFile::new()
        .expect("Could not create temporary file");
    let temp_file = temp_file.into_temp_path();
    test_details.write_json(&temp_file)
        .expect("Could not serialize book details to JSON");
}
