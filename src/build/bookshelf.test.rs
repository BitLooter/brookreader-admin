use chrono::Utc;
use tempfile::NamedTempFile;

use crate::mediadb::{Book, Shelf};
use crate::metadata::BookMetadata;


#[test]
fn bookshelf_smoke_test() {
    let mut bookshelf = super::Bookshelf::new_with_main("Test bookshelf", "Main");

    let root_shelf = Shelf::new("root", "Test root", "/no/path");
    bookshelf.push_shelf(root_shelf);

    let test_metadata = BookMetadata {
        title: "Test book".into(),
        date_added: Utc::now()
    };
    let test_book = Book::new("testbook", test_metadata, "no.url", "/no/path");
    bookshelf.push_book_to_shelf(test_book, "root")
        .expect("Could not add book to shelf");

    assert_eq!(2, bookshelf.shelves.len());
    assert_eq!(1, bookshelf.books.len());
    assert!(bookshelf.books.contains_key("testbook"));
    let test_shelf = bookshelf.shelves.get("root")
        .expect("Could not get shelf 'root'");
    assert!(test_shelf.books.contains(&"testbook".to_owned()));

    let temp_file = NamedTempFile::new()
        .expect("Could not create temporary file");
    let temp_file = temp_file.into_temp_path();
    bookshelf.write_json(&temp_file)
        .expect("Could not serialize bookshelf to JSON");
}
