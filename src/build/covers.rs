use std::{
    path::{Path, PathBuf},
};
use image::{DynamicImage, ImageError};
use snafu::{OptionExt, ResultExt, Snafu, ensure};

use crate::archive::open_archive;
use crate::config::Config;
use crate::mediadb::{Book, MediaDatabase, Shelf};


#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Could not get metadata for file '{}': {}", path.display(), source))]
    FSMetadata { path: PathBuf, source: std::io::Error },
    #[snafu(display("Could not read media archive '{}': {}", path.display(), source))]
    MediaArchiveRead { path: PathBuf, source: crate::archive::Error },
    #[snafu(display("No cover available for media '{}'", path.display() ))]
    NoCover { path: PathBuf },
    #[snafu(display("Could not read cover for media '{}': {}",
                    media_path.display(), source ))]
    CoverRead { media_path: PathBuf, source: image::ImageError },
    #[snafu(display("Could not write cover '{}' for media '{}': {}",
                    cover_path.display(), media_path.display(), source ))]
    CoverWriteError { cover_path: PathBuf, media_path: PathBuf, source: ImageError },
    #[snafu(display("Could not read image file '{}': {}", path.display(), source))]
    ImageRead { path: PathBuf, source: image::ImageError },
    #[snafu(display("Filename '{}' in archive '{}' not valid unicode",
        image_path.display(), archive_path.display()))]
    InvalidUnicode { image_path: PathBuf, archive_path: PathBuf },
    #[snafu(display("Could not determine type of file '{}' in archive '{}'",
        image_path.display(), archive_path.display()))]
    UnknownImageType { image_path: PathBuf, archive_path: PathBuf }
}
type Result<T, E = Error> = std::result::Result<T, E>;

/// Methods to handle reading/processing/writing cover images, along with
/// associated configuration
pub struct CoverManager<'config> {
    /// Directory containing outputted book covers
    book_cover_dir: &'config Path,
    /// Directory containing books
    book_root_dir: &'config Path,
    /// File extension to use for covers
    cover_ext: &'config str,
    /// File extension to use for covers
    cover_thumb_ext: &'config str,
    /// Maximun number of covers to use when generating shelf covers
    coverstack_max_count: usize,
    /// Maximun amount cover may be darked in a coverstack
    coverstack_max_darkness: i32,
    /// Maximun amount cover may be darked in a coverstack
    coverstack_scale: f32,
    /// List of filenames that may override a shelf's cover (e.g. cover.jpg)
    directory_cover_names: Vec<String>,
    /// Directory containing outputted shelf covers
    shelf_cover_dir: PathBuf,
    /// Height of thumbnails in pixels
    thumbnail_height: u32,
    /// Width of thumbnails in pixels
    thumbnail_width: u32,
}

impl<'config> CoverManager<'config> {
    // Create a CoverManager with the given configuration
    pub fn new(config: &'config Config) -> Self {
        Self {
            book_cover_dir: &config.build_cover_path,
            book_root_dir: &config.media_books_root,
            cover_ext: &config.cover_format,
            cover_thumb_ext: &config.cover_thumb_format,
            coverstack_max_count: 4,
            coverstack_max_darkness: -63,
            coverstack_scale: 2.0 / 3.0,
            directory_cover_names: vec!["cover.png".to_owned(), "cover.jpg".to_owned()],
            shelf_cover_dir: config.build_cover_path.join("shelves"),
            thumbnail_height: config.cover_thumbnail_height as u32,
            thumbnail_width: config.cover_thumbnail_width as u32,
        }
    }

    /// Check if cover needs to be extracted or updated and do so
    pub fn ensure_book_cover(&self, book: &Book) -> Result<()> {
        let cover_path = self.book_cover_dir
            .join(self.make_cover_filename(&book.id, "full", &self.cover_ext));
        let book_full_path = self.book_root_dir.join(&book.path);
        let archive_updated = check_cover_media_updated(&cover_path, &book_full_path)?;
        // Extract new cover if not present or if source was updated
        if !cover_path.exists() || archive_updated {
            let cover = self.get_book_cover_from_source(&book_full_path)?;
            cover.save(&cover_path)
                .context(CoverWriteError {cover_path, media_path: &book_full_path})?;
            let cover_thumb = cover.thumbnail(self.thumbnail_width, self.thumbnail_height);
            let cover_thumb_path = self.book_cover_dir
                .join(self.make_cover_filename(&book.id, "thumb", &self.cover_thumb_ext));
            cover_thumb.save(&cover_thumb_path)
                .context(CoverWriteError {cover_path: cover_thumb_path, media_path: &book_full_path})?;
        }
        Ok(())
    }

    /// Checks if shelf cover present and extract cover if needed. Need books
    /// for cover generation if needed.
    pub fn ensure_shelf_cover(&self, shelf: &Shelf, database: &MediaDatabase) -> Result<()> {
        let shelf_cover_path = self.shelf_cover_dir
            .join(self.make_cover_filename(&shelf.id, "thumb", &self.cover_thumb_ext));
        let shelf_full_path = self.book_root_dir.join(&shelf.path);
        let shelf_updated = check_cover_media_updated(&shelf_cover_path, &shelf_full_path)?;
        // Extract new cover if not present or if source was updated
        if !shelf_cover_path.exists() || shelf_updated {
            self.get_shelf_cover_from_source(shelf, database)?
                // TODO: Better image options (e.g. WebM, PNG compression settings)
                .save(&shelf_cover_path)
                .context(CoverWriteError {cover_path: &shelf_cover_path, media_path: &shelf_full_path})?;
        }
        Ok(())
    }

    /// Create a filename for a cover based on configured settings. cover_type
    /// describes the cover format (e.g. "full", "thumb") and cover_format is
    /// the file format ("jpg", "png", etc.)
    fn make_cover_filename(&self, media_id: &str, cover_type: &str, cover_format: &str) -> String {
        format!("{}-{}.{}", media_id, cover_type, cover_format)
    }

    // TODO: Rename to make it more explicit that it's generating data and not retrieving
    /// Generate a cover from book archive
    fn get_book_cover_from_source(&self, book_path: &Path) -> Result<DynamicImage> {
        let cover_file_data = extract_first_image_file(&book_path)?;
        let cover_image = image::load_from_memory(&cover_file_data)
            .context(CoverRead {media_path: &book_path})?;

        Ok(cover_image)
    }

    /// Get cover image from extracted covers or from source
    fn load_cover(&self, book: &Book) -> Result<DynamicImage> {
        let extracted_path = self.book_cover_dir
            .join(self.make_cover_filename(&book.id, "full", &self.cover_ext));
        let cover_image: DynamicImage;
        if extracted_path.exists() {
            // Use pre-extracted cover if one is present
            cover_image = image::open(&extracted_path)
                .context(ImageRead {path: &extracted_path})?;
        } else {
            // Otherwise read it from sources
            let book_path = self.book_root_dir.join(&book.path);
            cover_image = self.get_book_cover_from_source(&book_path)?;
        }
        Ok(cover_image)
    }

    /// Write a cover for a shelf to disk from a supplied cover or generate
    /// from contained books
    fn get_shelf_cover_from_source(&self, shelf: &Shelf, database: &MediaDatabase) -> Result<DynamicImage> {
        let cover: DynamicImage;
        if let Some(cover_path) = self.find_directory_cover_override(&shelf.path) {
            // Use existing cover override
            let cover_source = image::open(&cover_path)
                .context(ImageRead {path: cover_path})?;
            // TODO:UI: Generate full size shelf covers
            cover = cover_source.thumbnail(self.thumbnail_width, self.thumbnail_height);
        } else {
            // Create a cover from items on shelf
            cover = self.generate_shelf_cover(shelf, database)?;
        }
        Ok(cover)
    }

    /// Creates a cover for a shelf based on contained items
    fn generate_shelf_cover(&self, shelf: &Shelf, database: &MediaDatabase) -> Result<DynamicImage> {
        let mut books_for_stack: Vec<Book> = Vec::new();
        for book_id in shelf.books.iter() {
            books_for_stack.push(database.get_book(book_id)
                .expect("All book IDs in shelves should exist in books"));
            if books_for_stack.len() == self.coverstack_max_count {
                break;
            }
        }
        // Do not generate a blank stack
        ensure!(books_for_stack.len() > 0, NoCover {path: &shelf.path});

        let mut cover = DynamicImage::new_rgba8(self.thumbnail_width, self.thumbnail_height);
        let coverstack_thumb_width = (self.thumbnail_width as f32 * self.coverstack_scale) as u32;
        let coverstack_thumb_height = (self.thumbnail_height as f32 * self.coverstack_scale) as u32;
        let thumb_width_pad = (self.thumbnail_width - coverstack_thumb_width) as f32;
        let thumb_height_pad = (self.thumbnail_height - coverstack_thumb_height) as f32;
        for (i, book) in books_for_stack.iter().enumerate().rev() {
            let thumb_progress = 1.0 - (i as f32 / (self.coverstack_max_count - 1) as f32);
            let thumb_x = (thumb_width_pad - (thumb_width_pad * thumb_progress)) as u32;
            let thumb_y = (thumb_height_pad * thumb_progress) as u32;
            let thumb_brightness = (self.coverstack_max_darkness as f32 * (1.0 - thumb_progress)) as i32;

            let book_cover = self.load_cover(book)?
                .thumbnail(coverstack_thumb_width, coverstack_thumb_height)
                .brighten(thumb_brightness);
            image::imageops::replace(&mut cover, &book_cover, thumb_x, thumb_y);
        }

        Ok(cover)
    }

    /// Return path of cover override if present in directory
    fn find_directory_cover_override(&self, path: &Path) -> Option<PathBuf> {
        self.directory_cover_names
            .iter()
            .map(|n| path.join(n))
            .find(|p| p.exists())
    }
}

/// Checks timestamp of cover against media, and returns true if media is newer
///
/// Note that if media is a directory (shelf, bare dir, etc.) it will return
/// false if the files inside the directory have been modified but not renamed/
/// added/deleted. Books are normally pretty static so this isn't much of an
/// issue but in the future this function could check directory contents as
/// well.
fn check_cover_media_updated(cover_path: &Path, media_path: &Path) -> Result<bool> {
    let cover_time;
    if cover_path.exists() {
        cover_time = cover_path.metadata()
            .context(FSMetadata {path: &cover_path})?
            .modified()
            .context(FSMetadata {path: &cover_path})?;
    } else {
        cover_time = std::time::UNIX_EPOCH;
    }
    let media_time = media_path.metadata()
        .context(FSMetadata {path: &media_path})?
        .modified()
        .context(FSMetadata {path: &media_path})?;

    Ok(&media_time > &cover_time)
}

/// Find first image file (book cover) in archive and return its filename and contents
fn extract_first_image_file(archive_path: &Path) -> Result<Vec<u8>> {
    let archive = open_archive(archive_path)
        .context(MediaArchiveRead {path: &archive_path})?;
    let mut page_paths = archive.images()
        .context(MediaArchiveRead {path: &archive_path})?;
    page_paths.sort_unstable();

    let first_page_path = page_paths.get(0)
        .context(NoCover {path: &archive_path})?;
    let cover_data = archive.read_file_data(&first_page_path)
        .context(MediaArchiveRead {path: &archive_path})?;
    Ok(cover_data)
}


#[cfg(test)]
#[path = "./covers.test.rs"]
mod covers_test;
