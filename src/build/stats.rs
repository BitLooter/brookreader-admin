use std::{fs,
          path::{Path, PathBuf}};
use chrono::Utc;
use serde::{Serialize};
use snafu::{ResultExt, Snafu};

use crate::mediadb::MediaDatabase;


#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Could not serialize stats: {}", source))]
    JSONSerialize { source: serde_json::Error },
    #[snafu(display("Could not read file metadata for path {}: {}", path.display(), source))]
    ReadMetadata { path: PathBuf, source: std::io::Error },
    #[snafu(display("Could not read file information for {}: {}", path.display(), source))]
    StatGenerationError { path: PathBuf, source: std::io::Error },
    #[snafu(display("Could not write file {}: {}", path.display(), source))]
    WriteToDisk { path: PathBuf, source: std::io::Error },
}
type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Stats {
    pub library: DatabaseStats,
    pub server: ServerStats
}

impl Stats {
    /// Construct statistics from database
    pub fn new(database: &MediaDatabase, books_root: &Path) -> Result<Self> {
        Ok(Self {
            library: DatabaseStats::new(database, books_root)?,
            server: ServerStats::new()
        })
    }

    /// Write out stats to JSON
    pub fn write(&self, stats_path: &Path) -> Result<()> {
        let stats_json = serde_json::to_string_pretty(self)
            .context(JSONSerialize)?;
        fs::write(stats_path, stats_json)
            .context(WriteToDisk {path: stats_path})?;
        Ok(())
    }
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ServerStats {
    pub date: String,
    pub version: String
}

impl ServerStats {
    pub fn new() -> Self {
        let timestamp = Utc::now();
        Self {
            date: timestamp.to_rfc3339(),
            version: env!("CARGO_PKG_VERSION").to_owned()
        }
    }
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DatabaseStats {
    pub books_size: u64,
    pub books_total: usize
}

impl DatabaseStats {
    fn new(database: &MediaDatabase, books_root: &Path) -> Result<Self> {
        let books_size = database.get_all_books().try_fold(
            0, |last_size, book| {
                let book_metadata = books_root.join(&book.path).metadata()
                    .context( StatGenerationError {path: book.path})?;
                Ok(last_size + book_metadata.len())
            }
        )?;

        Ok(Self {
            books_size,
            books_total: database.num_books()
        })
    }
}
