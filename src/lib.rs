pub mod archive;
pub mod build;
pub mod commands;
pub mod logging;
pub mod mediadb;
pub mod metadata;
pub mod scan;
pub mod showcase;
pub mod ui;
pub mod util;
pub mod config;

pub use config::{Config, Error as ConfigError};


// Allow unit tests access to common test utils
#[cfg(test)]
#[path = "../tests/common/mod.rs"]
pub mod tests_common;
