use std::fs;

use crate::tests_common;


const TEST_LIBRARY_CONTENTS: &[&str] = &[
    "book_formats", "error_cases", "invisible", "ordered", "root_book.cbz", "test_cases"
];


#[test]
fn find_in_path_ancestors() {
    let root = tests_common::testenv_root();
    let found_path = super::find_in_path_ancestors(&root.join("static"), "Brookreader.toml");
    assert_eq!(Some(root.join("Brookreader.toml")), found_path);
    assert!(super::find_in_path_ancestors(&root, "not_a_real_file_anywhere").is_none());
}

#[test]
fn remove_trailing_slash() {
    assert_eq!("/a/b/c", super::remove_trailing_slash("/a/b/c/".to_owned()));
    assert_eq!("/a/b/c", super::remove_trailing_slash("/a/b/c".to_owned()));
}

#[test]
fn path_is_zip() {
    assert!(super::path_is_zip("example.cbz"));
    assert!(super::path_is_zip("example.zip"));
    assert!(super::path_is_zip("/abslute/path/example.cbz"));
    assert!(!super::path_is_zip("example.rar"));
}

#[test]
fn path_is_rar() {
    assert!(super::path_is_rar("example.cbr"));
    assert!(super::path_is_rar("example.rar"));
    assert!(super::path_is_rar("/abslute/path/example.rar"));
    assert!(!super::path_is_rar("example.zip"));
}

#[test]
fn path_is_book_file() {
    assert!(super::path_is_book_file("example.cbr"));
    assert!(super::path_is_book_file("example.cbz"));
    assert!(super::path_is_book_file("/abslute/path/example.cbz"));
    assert!(!super::path_is_book_file("example.png"));
}

#[test]
fn path_is_image() {
    assert!(super::path_is_image("example.gif"));
    assert!(super::path_is_image("example.jpg"));
    assert!(super::path_is_image("example.jpeg"));
    assert!(super::path_is_image("example.png"));
    assert!(super::path_is_image("/abslute/path/example.jpg"));
    assert!(!super::path_is_image("example.txt"));
    // Files with image extensions in a __MACOSX directory are not images
    assert!(!super::path_is_image("__MACOSX/example.png"));
}

#[test]
fn path_is_type() {
    assert!(super::path_is_type("example.ext", &["ext"]));
    assert!(super::path_is_type("/abslute/path/example.ext", &["ext"]));
    assert!(!super::path_is_type("example.not", &["ext"]));
    assert!(!super::path_is_type("example.ext", &[]));
    assert!(!super::path_is_type("example", &["ext"]));
}

#[test]
fn get_normalized_extension() {
    assert_eq!("ext", super::get_normalized_extension("example.ext"));
    assert_eq!("ext", super::get_normalized_extension("example.EXT"));
    assert_eq!("", super::get_normalized_extension("example"));
}

#[test]
fn copy_dir_contents() {
    let source_path = tests_common::testenv_root().join("static/books");
    let dest_path = tempfile::tempdir().expect("Could not create temporary directory");
    let dest_path = dest_path.path();

    super::copy_dir_contents(&source_path, &dest_path)
        .expect(&format!(
            "Failed to copy directory structure from {:?} to {:?}", &source_path, &dest_path
        ));

    for book_file in TEST_LIBRARY_CONTENTS {
        let books_path = dest_path.join(book_file);
        assert!(books_path.exists(), "Destination path was not created: {:?}", book_file);
    }
}

#[test]
fn link_dir_contents() {
    let source_path = tests_common::testenv_root().join("static/books");
    let dest_path = tempfile::tempdir().expect("Could not create temporary directory");
    let dest_path = dest_path.path();

    super::link_dir_contents(&source_path, &dest_path)
        .expect(&format!(
            "Failed to copy directory structure from {:?} to {:?}", &source_path, &dest_path
        ));

    for book_file in TEST_LIBRARY_CONTENTS {
        let books_path = dest_path.join(book_file);
        assert!(books_path.exists(), "Destination link does not exist: {:?}", book_file);
        assert!(
            fs::symlink_metadata(&books_path)
                .expect(&format!("Could not read metadata for path {:?}", &books_path))
                .file_type().is_symlink(),
            "Destination path not a symlink: {:?}", book_file
        );
    }
}

#[test]
fn dir_paths_iter() {
    let path = tests_common::testenv_root().join("static/books");
    let mut dir_paths = super::dir_paths_iter(&path)
        .expect(&format!("Could not get contents of directory {:?}", &path))
        .collect::<Vec<_>>();
    // Ensure output paths are ordered same as expected array for assert
    dir_paths.sort();
    let expected_paths: Vec<_> = TEST_LIBRARY_CONTENTS
        .iter()
        .map(|p| path.join(p))
        .collect();
    assert_eq!(expected_paths, dir_paths);
}

#[test]
fn encode_path_as_url() {
    assert_eq!("absolute/example.ext", super::encode_path_as_url("/absolute/example.ext"));
    assert_eq!("example.ext", super::encode_path_as_url("example.ext"));
}
