use std::{
    fs,
    io,
    path::{Path, PathBuf},
};
use chrono::{DateTime, Utc};
use regex::Regex;
use serde::{Deserialize, Serialize};
use snafu::{ResultExt, Snafu};

use crate::config::Config;


#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Could not deserialize metadata file {}: {}", path.display(), source))]
    MetadataDeserialize { path: PathBuf, source: toml::de::Error },
    #[snafu(display("Could not read metadata file {}: {}", path.display(), source))]
    MetadataRead { path: PathBuf, source: io::Error },
    #[snafu(display("Could not serialize metadata for {}: {}", path.display(), source))]
    MetadataSerialize { path: PathBuf, source: toml::ser::Error },
    #[snafu(display("Could not write metadata file {}: {}", path.display(), source))]
    MetadataWrite { path: PathBuf, source: io::Error },
}
type Result<T, E = Error> = std::result::Result<T, E>;

/** Read and parse metadata from media and metadata override files */
pub struct MetadataManager<'config> {
    metadata_base_path: &'config Path,
    regex_brackets: Regex,
    regex_whitespace: Regex,
}

impl<'config> MetadataManager<'config> {
    pub fn new(config: &'config Config) -> Self {
        // TODO: Make regexes configurable
        // Matches string inside () or [] and any whitespace after at end of string
        let regex_brackets = Regex::new(r"[\(\[][^\(\[]*[\)\]]\s*$")
            .expect("brackets_regex is invalid");
        // Matches . or _ to be replaced with spaces
        let regex_whitespace = Regex::new(r"[\._]")
            .expect("whitespace_regex is invalid");

        Self {
            metadata_base_path: &config.scan_metadata_path,
            regex_brackets,
            regex_whitespace,
        }
    }

    /// Load book metadata from available sources. metadata_base_path is where the
    /// corrosponding metadata overrides file may be found.
    pub fn get_book(&self, path: &Path, id: &str) -> Result<BookMetadata> {
        let metadata_toml_path = self.metadata_base_path.join(format!("{}.toml", id));
        let metadata_toml = BookMetadataToml::load_or_create(&metadata_toml_path)?;
        let title: String;
        if let Some(title_override) = metadata_toml.title {
            title = title_override;
        } else {
            // Book title is based on filename if not specified
            title = self.clean_title(
                &path.file_stem()
                    .expect("Book has no filename")
                    .to_string_lossy().into_owned()
            );
        }

        Ok(BookMetadata {
            title,
            date_added: metadata_toml.date_added
        })
    }

    /// Filter a filename to make a pretty title
    fn clean_title(&self, filename: &str) -> String {
        let mut title = String::from(filename);
        // TODO: Instead of manually trimming string, build a regex that matches
        //  everything before last bracket sets. Needs to allow bracket sets before
        //  end if there is more text after them. I tried to do this but regexes
        //  are hard.
        loop {
            let trimmed_title = self.regex_brackets.replace(&title, "");
            if title == trimmed_title {
                break;
            } else {
                title = trimmed_title.to_string();
            }
        }
        let title = self.regex_whitespace.replace(&title, " ");
        let title = title.trim();
        title.to_string()
    }
}

#[derive(Debug)]
pub struct BookMetadata {
    pub title: String,
    pub date_added: DateTime<Utc>,
}

/// Representation of TOML metadata overrides for a book
#[derive(Debug, Deserialize, Serialize)]
struct BookMetadataToml {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    pub date_added: DateTime<Utc>,
}

impl BookMetadataToml {
    /// Load metadata from the given path
    pub fn load(path: &Path) -> Result<Self> {
        let metadata_text = fs::read_to_string(path)
            .context(MetadataRead {path})?;
        let metadata: BookMetadataToml = toml::from_str(&metadata_text)
            .context(MetadataDeserialize {path})?;
        Ok(metadata)
    }

    /// Save metadata to given path. Optional fields skipped if not set.
    pub fn save(&self, path: &Path) -> Result<()> {
        let metadata_text = toml::to_string(self)
            .context(MetadataSerialize {path})?;
        fs::write(path, metadata_text)
            .context(MetadataWrite {path})?;
        Ok(())
    }

    // Loads an existing metadata file or create a default if not present
    pub fn load_or_create(path: &Path) -> Result<Self> {
        let metadata: Self;
        if path.exists() {
            metadata = BookMetadataToml::load(&path)?;
        } else {
            metadata = BookMetadataToml::default();
            metadata.save(&path)?;
        }
        Ok(metadata)
    }
}

impl Default for BookMetadataToml {
    /// Create a new TOML metadata structure
    fn default() -> Self {
        Self {
            title: None,
            date_added: Utc::now()
        }
    }
}


#[cfg(test)]
#[path = "./metadata.test.rs"]
mod metadata_test;
