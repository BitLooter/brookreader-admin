use std::{error, fmt, fs, io, path::{Path, PathBuf}};
use path_abs::{self, PathAbs};

use crate::build;
use crate::config::{self as config_mod, Config, ConfigFile};
use crate::mediadb::{self, MediaDatabase};
use crate::scan;
use crate::showcase::ShowcaseList;
use crate::ui;


const FEATURED_SHOWCASE_NAME: &str = "featured";
const NEW_CONFIG_NAME: &str = "Brookreader.toml";

/// Create website from scanned data
pub fn build(config: &Config, do_scan: bool) -> Result<()> {
    if do_scan {
        scan::scan(config)?;
    }
    build::build_data(config)?;
    build::build_fe(config)?;
    Ok(())
}

/// Add an item to the featured list
pub fn feature(source_paths: Vec<PathBuf>, config: &Config) -> Result<()> {
    // TODO: Add by ID as well
    config.ui_hooks.showcase_begin(FEATURED_SHOWCASE_NAME)
        .map_err(|e| Error::showcase_ui(&e))?;

    // Normalize and sanity check input
    let item_paths = source_paths.iter()
        .map(|p| PathAbs::new(&p)
            .map_err(|p_err| Error::showcase_bad_path(p_err))
            .and_then(|p_ok| {
                p_ok.as_path().strip_prefix(&config.media_books_root)
                    .map(|p_stripped| p_stripped.to_owned() )
                    .map_err(|_| Error::showcase_path_outside_media(p_ok.as_path(), &config.media_books_root))
            })
        )
        .collect::<Result<Vec<_>>>()?;

    let featured_index_path = config.base_path.join("featured.txt");
    let mut showcase_list = ShowcaseList::load(&featured_index_path)
        .map_err(|e| Error::showcase_list_load(&featured_index_path, &e))?;

    let database = MediaDatabase::open(&config.database_location)?;

    for item_path in item_paths {
        if let Some(media) = database.get_book_with_path(&item_path) {
            showcase_list.add_media(&media.id);
            config.ui_hooks.showcase_add_media(FEATURED_SHOWCASE_NAME, &media)
                .map_err(|e| Error::showcase_ui(&e))?;
        } else if let Some(shelf) = database.get_shelf_with_path(&item_path) {
            showcase_list.add_shelf(&shelf.id);
            config.ui_hooks.showcase_add_shelf(FEATURED_SHOWCASE_NAME, &shelf)
                .map_err(|e| Error::showcase_ui(&e))?;
        } else {
            return Err(Error::showcase_path_unknown(&item_path));
        }
    }

    showcase_list.save()
        .map_err(|e| Error::showcase_save(&e))?;
    config.ui_hooks.showcase_end(FEATURED_SHOWCASE_NAME)
        .map_err(|e| Error::showcase_ui(&e))?;
    Ok(())
}

pub fn init(init_path: &Path, new_config: &ConfigFile, ui_hooks: &Box<dyn ui::UserInterface>) -> Result<()> {
    let config_path = init_path.join(NEW_CONFIG_NAME);

    let mut config_path_iter = init_path.read_dir()
        .map_err(|e| Error::init_read(&init_path, &e))?;
    // UNSTABLE: bool::then()
    if config_path_iter.next().is_some() {
        return Err(Error::init_not_empty(init_path));
    } else {
        ui_hooks.init_begin(init_path, &config_path)
            .map_err(|e| Error::init_ui(&e))?;
        fs::create_dir_all(init_path)
            .map_err(|e| Error::init_create(init_path, &e))?;
        new_config.write_annotated_config(&config_path)
            .map_err(|e| Error::init_config(&config_path, &e))?;
        fs::create_dir_all(init_path.join("static/books"))
            .map_err(|e| Error::init_create(&init_path.join("static"), &e))?;
        fs::File::create(init_path.join("featured.txt"))
            .map_err(|e| Error::init_create(&init_path.join("featured.txt"), &e))?;
    }

    ui_hooks.init_end(init_path).map_err(|e| Error::init_ui(&e))?;
    Ok(())
}

pub fn scan(config: &Config) -> Result<()> {
    scan::scan(config)?;
    Ok(())
}

#[derive(Debug)]
pub enum Error {
    Build(build::Error),
    Database(mediadb::Error),
    Init(String),
    Scan(scan::Error),
    Showcase(String),
}
type Result<T, E = Error> = std::result::Result<T, E>;

impl Error {
    /// Error writing configuration file during init
    pub(self) fn init_config(path: &Path, err: &config_mod::Error) -> Self {
        Self::Init(
            format!(r#"Could not write new config file "{}": {}"#, path.display(), err)
        )
    }

    /// Error creating directory or file during init
    pub(self) fn init_create(path: &Path, err: &io::Error) -> Self {
        Self::Init(
            format!(r#"Could not create path "{}" for init: {}"#, path.display(), err)
        )
    }

    /// New init directory already contains files
    pub(self) fn init_not_empty(path: &Path) -> Self {
        Self::Init(format!(r#"Init directory "{}" not empty"#, path.display()))
    }

    /// Problem reading init directory
    pub(self) fn init_read(path: &Path, err: &io::Error) -> Self {
        Self::Init(
            format!(r#"Could not read contents of directory "{}": {}"#, path.display(), err)
        )
    }

    pub(self) fn init_ui(err: &ui::Error) -> Self {
        Self::Init(
            format!("UI error during init: {}", err)
        )
    }

    /// Path for showcase entry was invalid
    pub(self) fn showcase_bad_path(err: path_abs::Error) -> Self {
        Self::Showcase(format!(
            r#"Could not normalize path "{}": {}"#,
            err.path().display(),
            &err
        ))
    }

    /// Path for showcase entry not contained within media root
    pub(self) fn showcase_path_outside_media(path: &Path, media_root: &Path) -> Self {
        Self::Showcase(
            format!(
                r#"Cannot showcase "{}", must be contained in media directory ({})"#,
                path.display(),
                media_root.display()
            )
        )
    }

    /// Path for showcase entry not contained within media root
    pub(self) fn showcase_path_unknown(path: &Path) -> Self {
        Self::Showcase(
            format!(
                r#"Showcase path "{}" not found in database. If it was added recently you may need to add it to the database by performing a scan."#,
                path.display(),
            )
        )
    }

    /// Problem loading the showcase listing
    pub(self) fn showcase_list_load(path: &Path, err: &io::Error) -> Self {
        Self::Showcase(
            format!(r#"Could not load showcase list "{}": {}"#, path.display(), err)
        )
    }

    /// Problem saving the showcase listing
    pub(self) fn showcase_save(err: &io::Error) -> Self {
        Self::Showcase(format!("Could not save showcase: {}", err))
    }

    pub(self) fn showcase_ui(err: &ui::Error) -> Self {
        Self::Showcase(
            format!("UI error during showcase editing: {}", err)
        )
    }
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Build(err) => write!(f,
                "Could not build site: {}", err
            ),
            Self::Database(err) => write!(f,
                "Could not access database: {}", err
            ),
            Self::Init(msg) => msg.fmt(f),
            Self::Scan(err) => write!(f,
                "Could not complete scan: {}", err
            ),
            Self::Showcase(msg) => msg.fmt(f),
        }
    }
}

impl From<build::Error> for Error {
    fn from(err: build::Error) -> Self {
        Self::Build(err)
    }
}

impl From<mediadb::Error> for Error {
    fn from(err: mediadb::Error) -> Self {
        Self::Database(err)
    }
}

impl From<scan::Error> for Error {
    fn from(err: scan::Error) -> Self {
        Self::Scan(err)
    }
}
