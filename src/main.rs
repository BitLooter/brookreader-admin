#[macro_use]
extern crate clap;

use std::{env, fmt, fs, path::{Path, PathBuf}};
use clap::{Arg, ArgMatches, Error as ClapError, SubCommand};

use brookreader_admin::commands;
use brookreader_admin::Config;
use brookreader_admin::config::ConfigFile;
use brookreader_admin::util::find_in_path_ancestors;
use brookreader_admin::logging::Logger;


type MainResult<T = ()> = Result<T, MainError>;

const DEFAULT_CONFIG_FILENAME: &str = "Brookreader.toml";
const DEFAULT_INIT_PATH: &str = "./";
const DEFAULT_BOOKS_ROOT: &str = "static/books";
const DEFAULT_BOOKS_URL: &str = "books";
const CONFIG_ENV_NAME: &str = "BROOKREADER_CONFIG";

fn main() -> MainResult {
    let cwd = env::current_dir()
        .map_err(|e| MainError::unreadable_cwd_error(e))?;
    let default_config_path = find_in_path_ancestors(&cwd, DEFAULT_CONFIG_FILENAME)
        .unwrap_or(PathBuf::from(DEFAULT_CONFIG_FILENAME));
    let cli_matches = parse_cli(&default_config_path)?;

    // Load configuration and common setup unless subcommand does not require it
    let config = match cli_matches.subcommand_name().unwrap_or("none") {
        "init" | "none" => default_config()?,
        _ => {
            let loaded_config = load_config(&cli_matches)?;
            if !cli_matches.is_present("dump_config") {
                // Actions that change things go here
                create_directories(&loaded_config)?;
            }
            loaded_config
        }
    };

    if cli_matches.is_present("dump_config") {
        // TODO: Why is the config being loaded again?
        dump_config(&load_config(&cli_matches)?);
    } else {
        dispatch_command(&cli_matches, &config)?;
    }

    Ok(())
}

/// Use Clap to parse the command line parameters
fn parse_cli<'configpath>(default_config_path: &'configpath Path) -> MainResult<clap::ArgMatches<'configpath>> {
    // Empty environment variables (set with zero-length string) will be caught
    // by empty_values(), but Clap's error message will make it seem like a
    // problem with command line parameters. Check vars first.
    if let Some(env_config) = env::var_os(CONFIG_ENV_NAME) {
        if env_config.is_empty() {
            return Err(MainError::env_config_empty());
        }
    }

    // Not using clap_app!, as it does not work well with string arguments more
    // complex than a single alphanumeric token.
    let cli_matches = app_from_crate!()
        .about("Management tool for Brookreader")
        .arg(Arg::with_name("config")
            .short("c").long("config")
            .help(&format!(
                "Configuration file location. If not specified tries to load \
                from the path contained in the environment variable \
                BROOKREADER_CONFIG. If neither the command-line option nor the \
                environment variable is set, searches parent directories for a \
                file named {}.",
                DEFAULT_CONFIG_FILENAME))
            .next_line_help(true)
            .default_value_os(&default_config_path.clone().as_os_str())
            .hide_default_value(true)
            .empty_values(false)
            .env(CONFIG_ENV_NAME)
        )
        .arg(Arg::with_name("dump_config")
            .long("dump-config")
            .help("Print current configuration and exits")
        )
        .subcommand(SubCommand::with_name("build")
            .about("Build website")
            .arg(Arg::with_name("scan")
                .short("s").long("scan")
                .help("Perform a scan before building")
            )
        )
        .subcommand(SubCommand::with_name("feature")
            .about("Add a book to featured items")
            .arg(Arg::with_name("FILE")
                .help("Path of the item to add")
                .required(true)
                .multiple(true)
            )
        )
        .subcommand(SubCommand::with_name("init")
            .about("Initialize new default project")
            .arg(Arg::with_name("DIR")
                .help("Destination directory of new project")
                .default_value(DEFAULT_INIT_PATH)
            )
            .arg(Arg::with_name("books_root")
                .long("books-root")
                .help("Root directory for books")
                .default_value(DEFAULT_BOOKS_ROOT)
            )
            .arg(Arg::with_name("books_url")
                .long("books-url")
                .help("Root URL for books")
                .default_value(DEFAULT_BOOKS_URL)
            )
        )
        .subcommand(SubCommand::with_name("scan")
            .about("Scan for books"))
    .get_matches();

    Ok(cli_matches)
}

/// Creates a default config for unconfigured commands
fn default_config() -> MainResult<Config> {
    let log_path = Path::new("/tmp/brookreader.log");
    let mut config = Config::default();
    config.set_ui_hooks( Logger::new_with_path(&log_path) );

    Ok(config)
}

/// Loads and prepares local configuration
fn load_config(cli_matches: &ArgMatches) -> MainResult<Config> {
    let config_path = value_t!(cli_matches, "config", std::path::PathBuf)
        .map_err(|e| MainError::invalid_config_path_error(e))?;
    if !config_path.exists() {
        return Err(MainError::no_config_found());
    }

    println!("Loading configuration from {}\n", config_path.display());
    let mut config = Config::load(&config_path, cli_matches)
        .map_err(|e| MainError::load_config_error(e))?;
    config.set_ui_hooks( Logger::new_with_path(&config.log_path) );

    Ok(config)
}

/// Create output directories
fn create_directories(config: &Config) -> MainResult {
    let mut directories = Vec::new();
    let details_dir = config.brookreader_data_path.join("details");
    let shelfcover_dir = config.build_cover_path.join("shelves");
    directories.push(&details_dir);
    directories.push(&shelfcover_dir);
    directories.push(&config.build_output_path);
    directories.push(&config.scan_metadata_path);
    for path in directories {
        fs::create_dir_all(path)
            .map_err(|e| MainError::directory_creation(e, path))?;
    }
    Ok(())
}

/// Print the configuration to the screen
fn dump_config(config: &Config) {
    println!(
        "Current configuration:
    base_path: {}
    brookreader_data_path: {}
    brookreader_data_url: {}
    build_frontend_path: {:?}
    build_output_path: {}
    build_static_path: {:?}
    build_static_symlink: {}
    database_location: {}
    log_path: {}
    media_books_root: {}
    media_books_url: {}
    scan_id_type_media: {}
    scan_id_type_shelf: {}
    showcase_recent_threshold: {}
    showcase_sections: {:?}
    ",
        config.base_path.display(),
        config.brookreader_data_path.display(),
        &config.brookreader_data_url,
        config.build_frontend_path,
        config.build_output_path.display(),
        config.build_static_path,
        config.build_static_symlink,
        config.database_location.display(),
        config.log_path.display(),
        config.media_books_root.display(),
        &config.media_books_url,
        config.scan_id_type_media,
        config.scan_id_type_shelf,
        config.showcase_recent_threshold,
        config.showcase_sections
    )
}

/// Execute the command based on the parsed command line parameters
fn dispatch_command(cli_matches: &ArgMatches, config: &Config) -> MainResult {
    let subcommand = cli_matches.subcommand_name()
        .unwrap_or("none");
    match subcommand {
        "build" => {
            let build_cli_matches = cli_matches.subcommand_matches(&subcommand)
                .expect(&format!("Error parsing command line for {} subcommand", &subcommand));
            commands::build(&config, build_cli_matches.is_present("scan"))?;
        }
        "feature" => {
            let feature_cli_matches = cli_matches.subcommand_matches(&subcommand)
                .expect(&format!("Error parsing command line for {} subcommand", &subcommand));
            let featured_paths = values_t!(feature_cli_matches, "FILE", std::path::PathBuf)
                .map_err(|e| MainError::new(
                    &format!("Unable to parse featured file path(s): {}", e)
                ))?;
            commands::feature(featured_paths, &config)?;
        }
        "init" => {
            let init_cli_matches = cli_matches.subcommand_matches(&subcommand)
                .expect(&format!("Error parsing command line for {} subcommand", &subcommand));
            let dest_path = value_t!(init_cli_matches, "DIR", std::path::PathBuf)
                .map_err(|e| MainError::new(
                    &format!("Unable to parse init directory path: {}", e)
                ))?;

            let mut new_config = ConfigFile::default();
            new_config.media_books_root = value_t!(init_cli_matches, "books_root", PathBuf)
                .map_err(|e| MainError::config_invalid_path_error("books_root", e))?;
            new_config.media_books_url = value_t!(init_cli_matches, "books_url", String)
                .map_err(|e| MainError::new(
                    &format!("Value for {} is invalid: {}", "books_url", e))
                )?;

            commands::init(&dest_path, &new_config, &config.ui_hooks)?;
        }
        "scan" => {
            commands::scan(&config)?;
        }
        "none" => {
            return Err(MainError::no_subcommand());
        }
        // Should never reach this point unless a subcommand was missed
        _ => panic!("Unimplemented subcommand given: {}", subcommand)
    }

    config.ui_hooks.operation_end()
        .map_err(|e| MainError::new(
            &format!("Could not output operation summary: {}", e)
        ))?;

    Ok(())
}

/// Errors returned by main()
struct MainError {
    message: String,
}

impl MainError {
    /// Create a new error with the given message
    fn new<S: Into<String>>(message: S) -> Self {
        Self {
            message: message.into(),
        }
    }

    /// Generate error for env variable for config file is set to empty string
    fn env_config_empty() -> Self {
        Self::new(
            &format!(
                "Environment variable {} is empty; specify configuration file \
                path wtih --config, set variable to valid path, or unset \
                variable to proceed.",
                CONFIG_ENV_NAME
            ),
        )
    }

    /// Generate error for no subcommand given
    fn no_subcommand() -> Self {
        let exe_name = env::current_exe()
            .expect("Can't get executable name");
        let exe_name = exe_name
            .file_name()
            .expect("Unable to parse executable name")
            .to_string_lossy();
        Self::new(
            &format!(
                "No subcommand given. Use '{} help' to see available commands.",
                &exe_name
            )
        )
    }

    /// A directory could not be created
    fn directory_creation(e: std::io::Error, path: &Path) -> Self {
        Self::new(
            &format!(
                "Unable to create directory {}: {}",
                path.display(),
                e
            ),
        )
    }

    /// Generate error for unable to locate config file
    fn no_config_found() -> Self {
        Self::new(
            &format!(
                "No configuration file found in parent directories or \
                {} environment variable.",
                CONFIG_ENV_NAME
            ),
        )
    }

    /// Generates (unwrapped) error for unable to read CWD
    fn unreadable_cwd_error(e: std::io::Error) -> Self {
        Self::new(&format!("Cannot read current directory: {}", e))
    }

    /// Generates (unwrapped) error for bad config file path from CLI
    fn invalid_config_path_error(e: ClapError) -> Self {
        Self::new(&format!("Unable to parse configuration path: {}", e))
    }

    /// Generates error for invalid path in CLI argument
    fn config_invalid_path_error(option_name: &str, e: ClapError) -> Self {
        Self::new(&format!("Invalid path for {}: {}", option_name, e))
    }

    /// Generate (unwrapped) error for problems loading configuration
    fn load_config_error(e: brookreader_admin::ConfigError) -> Self {
        Self::new(&format!("Error loading configuration: {}", e))
    }
}

impl fmt::Display for MainError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", &self.message)
    }
}

impl fmt::Debug for MainError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", &self.message)
    }
}

impl std::error::Error for MainError {}

impl From<commands::Error> for MainError {
    fn from(error: commands::Error) -> Self {
        Self::new(&format!("{}", error))
    }
}
