use std::path::Path;
use crate::mediadb::{Book, Shelf};
use crate::build::stats::Stats;


pub enum UiAction {
    Create,
    Skip,
    Update,
}

pub enum UiMediaType {
    Book,
}

// Error may become an associated type on UserInterface if io::Error no longer
// covers all UI errors
pub type Error = std::io::Error;
pub type Result<T, E = Error> = std::result::Result<T, E>;

/**
Hooks for providing activity information to the user interface. Impl this trait
and override any hooks you want to handle to take feedback from library code.
*/
pub trait UserInterface: std::fmt::Debug {
    // Hooks are defined by default as no-op functions. Code that uses this
    // trait can impliment handling code for as many or as few of the hooks as
    // needed.

    /// Start of build data phase (book index, cover extraction, etc.)
    fn build_data_begin(&self) -> Result<()> { Ok(()) }
    /// Extracting/generating a cover for the given media
    fn build_data_cover_media(&self, _media: &Book) -> Result<()> { Ok(()) }
    /// An error occurred extracting media cover
    fn build_data_cover_media_error(&self, _media: &Book, _message: &str) -> Result<()> { Ok(()) }
    /// Extracting/generating a cover for the given shelf
    fn build_data_cover_shelf(&self, _shelf: &Shelf) -> Result<()> { Ok(()) }
    /// An error occurred extracting shelf cover
    fn build_data_cover_shelf_error(&self, _shelf: &Shelf, _message: &str) -> Result<()> { Ok(()) }
    /// Starting extracting/generating covers
    fn build_data_covers_begin(&self) -> Result<()> { Ok(()) }
    /// Finished extracting/generating covers
    fn build_data_covers_end(&self) -> Result<()> { Ok(()) }
    /// Show statistics about scan
    fn build_data_stats(&self, _stats: &Stats) -> Result<()> { Ok(()) }
    /// End of build data phase
    fn build_data_end(&self) -> Result<()> { Ok(()) }
    /// Start of build frontend phase (frontend code and static data)
    fn build_fe_begin(&self) -> Result<()> { Ok(()) }
    /// End of build frontend phase
    fn build_fe_end(&self) -> Result<()> { Ok(()) }

    /// Started initializing new site
    fn init_begin(&self, _init_path: &Path, _config_path: &Path) -> Result<()> { Ok(()) }
    /// Finished initializing new site
    fn init_end(&self, _init_path: &Path) -> Result<()> { Ok(()) }

    /// Started running a commmand
    fn operation_begin(&self) -> Result<()> { Ok(()) }
    /// Finished running a command
    fn operation_end(&self) -> Result<()> { Ok(()) }

    /// Scan of root_path has started
    fn scan_begin(&self, _scan_path: &Path) -> Result<()> { Ok(()) }
    /// Scan of root_path has finished
    fn scan_end(&self, _scan_path: &Path) -> Result<()> { Ok(()) }
    /// Started media discovery (all media)
    fn scan_media_discovery_begin(&self, _scan_path: &Path) -> Result<()> { Ok(()) }
    /// Finished media discovery
    fn scan_media_discovery_end(&self, _scan_path: &Path) -> Result<()> { Ok(()) }
    /// Generating an ID for a media item (mostly useful for content hash IDs)
    fn scan_media_id_gen(&self, _scan_path: &Path, _media_path: &Path) -> Result<()> { Ok(()) }
    /// Started scanning a media item
    fn scan_media_item_begin(&self, _scan_path: &Path, _media_path: &Path, _media_type: UiMediaType) -> Result<()> { Ok(()) }
    /// Finished scanning a book
    fn scan_media_item_end(&self, _scan_path: &Path, _media_path: &Path, _media_id: &str, _scan_action: UiAction) -> Result<()> { Ok(()) }
    /// Finding book metadata
    fn scan_media_metadata(&self, _scan_path: &Path, _media_path: &Path) -> Result<()> { Ok(()) }
    /// Scan started postprocessing data
    fn scan_postprocessing(&self, _scan_path: &Path) -> Result<()> { Ok(()) }
    /// Removed a media item from database
    fn scan_prune_media(&self, _scan_path: &Path, _media: &Book) -> Result<()> { Ok(()) }
    /// Removed a shelf from database
    fn scan_prune_shelf(&self, _scan_path: &Path, _shelf: &Shelf) -> Result<()> { Ok(()) }
    /// Started shelf discovery
    fn scan_shelf_discovery_begin(&self, _scan_path: &Path) -> Result<()> { Ok(()) }
    /// Finished shelf discovery
    fn scan_shelf_discovery_end(&self, _scan_path: &Path) -> Result<()> { Ok(()) }

    /// Adding a media item to the showcase
    fn showcase_add_media(&self, _showcase_name: &str, _media: &Book) -> Result<()> { Ok(()) }
    /// Adding a shelf to the showcase
    fn showcase_add_shelf(&self, _showcase_name: &str, _shelf: &Shelf) -> Result<()> { Ok(()) }
    /// Started editing showcase with name
    fn showcase_begin(&self, _showcase_name: &str) -> Result<()> { Ok(()) }
    /// Finished editing showcase
    fn showcase_end(&self, _showcase_name: &str) -> Result<()> { Ok(()) }
}

/// An impl of UserInterface that doesn't do anything
#[derive(Debug)]
pub struct UserInterfaceNull {}

impl UserInterface for UserInterfaceNull {}

impl Default for UserInterfaceNull {
    fn default() -> Self {
        Self {}
    }
}
