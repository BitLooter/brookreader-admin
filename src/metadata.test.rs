use clap::ArgMatches;
use insta::{assert_debug_snapshot, assert_yaml_snapshot};

use crate::config::Config;
use crate::tests_common;


#[test]
fn read_metadata_file() {
    let metadata_path = tests_common::testenv_root().join("metadata/test-store.toml");

    let parsed_metadata = super::BookMetadataToml::load(&metadata_path)
        .expect(&format!("Could not load metadata file {:?}", metadata_path));

    assert_yaml_snapshot!("Parsed TOML", parsed_metadata);
}

#[test]
fn read_with_manager() {
    // TODO: Create tests_common::test_configuration()?
    let args = ArgMatches::new();
    let root_path = tests_common::testenv_root();
    let config_path = root_path.join("Brookreader.toml");
    let book_path = root_path.join("static/books/test_cases/test-store.cbz");
    let config = Config::load(&config_path, &args)
        .expect(&format!("Could not load configuration from {:?}", &config_path));
    let manager = super::MetadataManager::new(&config);

    // TODO: Change this to a book with a mix of metadata sources when implemented
    let meta = manager.get_book(&book_path, "test-store")
        .expect("Could not get metadata for book 'test-store'");

    assert_debug_snapshot!("Loaded metadata", meta);
}

#[test]
fn clean_title_from_filename() {
    let args = ArgMatches::new();
    let root_path = tests_common::testenv_root();
    let config_path = root_path.join("Brookreader.toml");
    let config = Config::load(&config_path, &args)
        .expect(&format!("Could not load configuration from {:?}", &config_path));
    let manager = super::MetadataManager::new(&config);

    // TODO: More tests when title cleaning is expanded on
    assert_eq!("Book title", manager.clean_title("Book title (With) [Extra] (stuff)"));
}
