use std::{ffi::OsStr, fs, io, path::{Path, PathBuf}};
use snafu::{ResultExt, Snafu};
use url::Url;


#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Unable to copy contents of {} to {}: {}", source_path.display(), dest_path.display(), source))]
    DirCopyError { source_path: PathBuf, dest_path: PathBuf, source: fs_extra::error::Error },
    #[snafu(display("Could not symlink {} to {}: {}", source_path.display(), dest_path.display(), source))]
    PathLinkError { source_path: PathBuf, dest_path: PathBuf, source: io::Error },
    #[snafu(display("Unable to read path {}: {}", path.display(), source))]
    PathReadError { path: PathBuf, source: io::Error },
}
type Result<T, E = Error> = std::result::Result<T, E>;

/// Searches path's ancestors for the given filename. Return PathBuf of the
/// file if found, otherwise None.
pub fn find_in_path_ancestors(path: &Path, filename: &str) -> Option<PathBuf> {
  for ancestor_path in path.ancestors() {
    let test_pathbuf = ancestor_path.join(filename);
    if test_pathbuf.exists() {
      return Some(test_pathbuf);
    }
  }

  // Filename not found
  None
}

/// Takes a string and removes trailing slash if present. To help ensure the
/// "wrong" path isn't used later, take ownership of the input string.
pub fn remove_trailing_slash(path: String) -> String {
    if path.ends_with("/") {
        path[..(path.len()-1)].to_string()
    } else {
        path
    }
}

/* Path type identification
 ***************************/
const FILETYPES_BOOKS: &[&str] = &["cbr", "cbz"];
const FILETYPES_RAR: &[&str] = &["rar", "cbr"];
const FILETYPES_ZIP: &[&str] = &["zip", "cbz"];
const FILETYPES_IMAGES: &[&str] = &["gif", "jpeg", "jpg", "png"];

/// Checks if path is a RAR file
pub fn path_is_rar<P: AsRef<Path>>(path: P) -> bool {
    path_is_type(path, FILETYPES_RAR)
}

/// Checks if path is a ZIP file
pub fn path_is_zip<P: AsRef<Path>>(path: P) -> bool {
    path_is_type(path, FILETYPES_ZIP)
}

/// Checks if path is a ZIP file
pub fn path_is_book_file<P: AsRef<Path>>(path: P) -> bool {
    path_is_type(path, FILETYPES_BOOKS)
}

/// Checks if path is any image file
pub fn path_is_image<P: AsRef<Path>>(path: P) -> bool {
    let path = path.as_ref();
    let valid_ext = path_is_type(path, FILETYPES_IMAGES);
    // __MACOSX is a directory used by MacOS that contains image metadata. The files contained
    // within have the same names as the images but are not valid images.
    let invalid_prefix = path.starts_with("__MACOSX");
    valid_ext && !invalid_prefix
}

/// Checks if path's extension matches any of the file types
fn path_is_type<P: AsRef<Path>>(path: P, filetypes: &[&str]) -> bool {
    let ext = path.as_ref().extension()
        .unwrap_or(OsStr::new(""))
        // All file types recognized by this program use simple ASCII file
        // extensions. If you have files with non-unicode extensions you have
        // bigger problems.
        .to_string_lossy()
        .to_lowercase();
    let ext = ext.as_str();
    filetypes.contains(&ext)
}

/** Returns file extension normalized to a lowercase string. Returns an empty
string if no extension, or if extension was not unicode. I am not aware of any
ebook formats that use a non-unicode extension, or for that matter any file
format that uses one, so I doubt this will be an issue. If this function is
causing you problems because it's returning None instead of your weird invalid
file extensions you probably deserve what you get. */
pub fn get_normalized_extension<P: AsRef<Path>>(path: P) -> String {
    path.as_ref().extension()
        .unwrap_or(OsStr::new(""))
        .to_str()
        .map(|s| s.to_lowercase())
        .unwrap_or(String::new())
}

/// Recursively copy the contents of directory source to dest
pub fn copy_dir_contents(source: &Path, dest: &Path) -> Result<()> {
    let source_contents: Vec<PathBuf> = dir_paths_iter(source)
        .context(PathReadError {path: source})?
        .collect();

    let mut copy_options = fs_extra::dir::CopyOptions::new();
    copy_options.overwrite = true;
    fs_extra::copy_items(&source_contents, dest, &copy_options)
        .context(DirCopyError {source_path: source, dest_path: dest})?;

    Ok(())
}

/// Create symlinks in dest dir to contents of source dir
pub fn link_dir_contents(source: &Path, dest: &Path) -> Result<()> {
    let source_contents = dir_paths_iter(source)
        .context(PathReadError {path: source})?
        .map(|p| p.strip_prefix(source).expect("Path should start with source").to_path_buf())
        .collect::<Vec<PathBuf>>();

    for source_item in source_contents {
        // TODO:WINDOWS: Support symlinks on Windows
        let item_source = source.join(&source_item);
        let item_dest = dest.join(&source_item);
        std::os::unix::fs::symlink(&item_source, &item_dest)
            .context(PathLinkError {source_path: item_source, dest_path: item_dest})?;
    }

    Ok(())
}

/// Returns iterator over paths in a given directory
pub fn dir_paths_iter(path: &Path) -> Result<impl Iterator<Item = PathBuf>, io::Error> {
    let read_result = fs::read_dir(path)?
        .collect::<io::Result<Vec<_>>>()?;

    Ok(read_result
        .into_iter()
        .map(|d| d.path() )
    )
}

/// Convert a file path to a relative URL
pub fn encode_path_as_url<P: AsRef<Path>>(path: P) -> String {
    // URL crate only works with absolute paths
    let path_as_absolute = Path::new("/").join(path.as_ref());
    let path_encoded = Url::from_file_path(path_as_absolute)
        .expect("File path is not absolute");
    // Strip out file:/// prefix
    let path_encoded = &path_encoded.as_str()[8..];
    path_encoded.to_owned()
}


#[cfg(test)]
#[path = "./util.test.rs"]
mod util_test;
