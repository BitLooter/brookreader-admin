use crate::tests_common;


#[test]
fn read_showcase_file() {
    let showcase_path = tests_common::testenv_root().join("featured.txt");

    let parsed_showcase = super::ShowcaseList::load(&showcase_path)
        .expect(&format!("Could not load showcase file {:?}", showcase_path));

    assert_eq!(2, parsed_showcase.items.len());
    assert!(parsed_showcase.items.contains("test-cbz"));
    assert!(parsed_showcase.items.contains("test-cbz"));
    assert_eq!(1, parsed_showcase.shelves.len());
    assert!(parsed_showcase.shelves.contains("test_cases"));
}
