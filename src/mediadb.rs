use std::{collections::HashMap,
          fs::File,
          io::BufReader,
          path::{Path, PathBuf}};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use snafu::{OptionExt, ResultExt, Snafu};

use crate::metadata::BookMetadata;


const CURRENT_DATABASE_VERSION: usize = 0;
pub const MAIN_SHELF_ID: &str = "main";

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Could not deserialize bookshelf from JSON: {}", source))]
    BookshelfJsonDeserialize { source: serde_json::error::Error },
    #[snafu(display("Could not serialize bookshelf to JSON: {}", source))]
    BookshelfJsonSerialize { source: serde_json::error::Error },
    #[snafu(display("Could not write bookshelf JSON to {}: {}", path.display(), source))]
    BookshelfJsonWrite { path: PathBuf, source: std::io::Error },
    #[snafu(display("Could not read bookshelf JSON from {}: {}", path.display(), source))]
    BookshelfJsonRead { path: PathBuf, source: std::io::Error },
    #[snafu(display("Shelf with ID {} not found in bookshelf", id))]
    ShelfNotFound { id: String },
}
type Result<T, E = Error> = std::result::Result<T, E>;

/** Main interface to the media database.

Contains methods for loading and inserting data into the database.

TODO:DATABASE: Currently reads/writes to a JSON file and holds all data in
memory. Will need refactoring to query an actual database.
*/
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct MediaDatabase {
    pub info: DatabaseInfo,
    shelves: HashMap<String, Shelf>,
    books: HashMap<String, Book>,
}

impl MediaDatabase {
    /** Create a new empty media database. The `id_type_*` parameters should be the
        algorithms used to generate IDs from the current configuration.
    */
    pub fn new<SMedia, SShelf>(id_type_media: SMedia, id_type_shelf: SShelf) -> Self
    where SMedia: Into<String>, SShelf: Into<String> {
        Self {
            info: DatabaseInfo {
                database_version: CURRENT_DATABASE_VERSION,
                id_type_media: id_type_media.into(),
                id_type_shelf: id_type_shelf.into()
            },
            shelves: HashMap::new(),
            books: HashMap::new()
        }
    }

    /// Write out database as JSON to path
    pub fn write_json(&self, bookshelf_path: &Path) -> Result<()> {
        // TODO:DATABASE: Will be removed once a real database is implemented
        let json_file = File::create(bookshelf_path)
            .context(BookshelfJsonWrite {path: bookshelf_path})?;
        serde_json::to_writer(&json_file, self)
            .context(BookshelfJsonSerialize {})?;
        Ok(())
    }

    /// Open the database at the given path
    pub fn open<P: AsRef<Path>>(path: P) -> Result<Self> {
        let path = path.as_ref();
        let file = File::open(path)
            .context(BookshelfJsonRead {path})?;
        let file_reader = BufReader::new(file);
        let database = serde_json::from_reader(file_reader)
            .context(BookshelfJsonDeserialize {})?;
        Ok(database)
    }

    // Database request methods

    /// Returns an iterator over all books
    pub fn get_all_books(&self) -> impl Iterator<Item = Book> + '_ {
        // TODO:DATABASE: In the future this will be the result of a database
        // query, rather than copying an existing structure. This applies to
        // all getters in this struct.
        self.books.values()
            .map(|b| Book {
                id: b.id.clone(),
                title: b.title.clone(),
                date_added: b.date_added.clone(),
                date_modified: b.date_modified.clone(),
                url: b.url.clone(),
                page_urls: b.page_urls.clone(),
                prev_id: b.prev_id.clone(),
                next_id: b.next_id.clone(),
                path: b.path.clone(),
            })
    }

    /// Returns an iterator over all books
    pub fn get_all_books_added_after(&self, earliest_date: DateTime<Utc>) -> impl Iterator<Item = Book> + '_ {
        self.books.values()
            .filter(move |b| b.date_added > earliest_date)
            .map(|b| Book {
                id: b.id.clone(),
                title: b.title.clone(),
                date_added: b.date_added.clone(),
                date_modified: b.date_modified.clone(),
                url: b.url.clone(),
                page_urls: b.page_urls.clone(),
                prev_id: b.prev_id.clone(),
                next_id: b.next_id.clone(),
                path: b.path.clone(),
            })
    }

    /// Returns an iterator over all book IDs in database
    pub fn get_all_book_ids(&self) -> impl Iterator<Item = String> + '_ {
        self.books.keys()
            .map(|i| i.to_owned())
    }

    /// Returns an iterator over all book IDs in database
    pub fn get_all_shelf_ids(&self) -> impl Iterator<Item = String> + '_ {
        self.shelves.keys()
            .map(|i| i.to_owned())
    }

    /// Returns an iterator over all shelves
    pub fn get_all_shelves(&self) -> impl Iterator<Item = Shelf> + '_ {
        self.shelves.values()
            .map(|s| Shelf {
                name: s.name.clone(),
                path: s.path.clone(),
                subshelves: s.subshelves.clone(),
                books: s.books.clone(),
                id: s.id.clone(),
            })
    }

    /// Return the book with the given ID, or None if not in database
    pub fn get_book<S: AsRef<str>>(&self, id: S) -> Option<Book> {
        self.books.get(id.as_ref())
            .map(|b| Book {
                id: b.id.clone(),
                title: b.title.clone(),
                date_added: b.date_added.clone(),
                date_modified: b.date_modified.clone(),
                url: b.url.clone(),
                page_urls: b.page_urls.clone(),
                prev_id: b.prev_id.clone(),
                next_id: b.next_id.clone(),
                path: b.path.clone(),
            })
    }

    /// Return the book found at the given file path
    pub fn get_book_with_path<P: AsRef<Path>>(&self, path: P) -> Option<Book> {
        self.books.values().find_map(|b| {
            if b.path == path.as_ref() {
                return Some(Book {
                    id: b.id.clone(),
                    title: b.title.clone(),
                    date_added: b.date_added.clone(),
                    date_modified: b.date_modified.clone(),
                    url: b.url.clone(),
                    page_urls: b.page_urls.clone(),
                    prev_id: b.prev_id.clone(),
                    next_id: b.next_id.clone(),
                    path: b.path.clone(),
                })
            }
            None
        })
    }

    /// Return the shelf with the given ID, or None if not in database
    pub fn get_shelf<S: AsRef<str>>(&self, id: S) -> Option<Shelf> {
        self.shelves.get(id.as_ref())
            .map(|s| Shelf {
                name: s.name.clone(),
                path: s.path.clone(),
                subshelves: s.subshelves.clone(),
                books: s.books.clone(),
                id: s.id.clone(),
            })
    }

    /// Return the shelf found at the given directory path
    pub fn get_shelf_with_path<P: AsRef<Path>>(&self, path: P) -> Option<Shelf> {
        let path = path.as_ref();
        self.shelves.values().find_map(|s| {
            if s.path == path {
                return Some(Shelf {
                    name: s.name.clone(),
                    path: s.path.clone(),
                    subshelves: s.subshelves.clone(),
                    books: s.books.clone(),
                    id: s.id.clone(),
                })
            }
            None
        })
    }

    /// Total number of books in database
    pub fn num_books(&self) -> usize {
        self.books.len()
    }

    /// Total number of books in database
    pub fn num_shelves(&self) -> usize {
        self.shelves.len()
    }

    // Database write methods

    /** Adds a Book to the database, or updates an existing one if one with its
    ID already present. */
    pub fn add_or_update_book(&mut self, book: Book) -> Result<()> {
        // TODO: Could the keys be a ref to book.id instead of cloning?
        self.books.insert(book.id.clone(), book);

        Ok(())
    }

    /// Create and add a shelf to the database
    pub fn create_shelf<P>(&mut self, name: &str, id: &str, path: P, parent: Option<&str>) -> Result<()>
    where P: AsRef<Path> {
        let shelf_id = id.to_string();
        // TODO: Maybe check if parent_id is valid and error if not?
        if let Some(parent_id) = parent {
            self.shelves.get_mut(parent_id)
                .context(ShelfNotFound {id: parent_id})?
                .subshelves.push(shelf_id.clone());
        }
        self.shelves.insert(shelf_id, Shelf::new(id, name, path.as_ref()));

        Ok(())
    }

    /// Remove a book from the database
    pub fn delete_book<S: AsRef<str>>(&mut self, id: S) -> Result<()> {
        self.books.remove(id.as_ref());
        Ok(())
    }

    /// Remove a shelf from the database
    pub fn delete_shelf<S: AsRef<str>>(&mut self, id: S) -> Result<()> {
        self.shelves.remove(id.as_ref());
        Ok(())
    }

    pub fn set_shelf_contents<SId, SMId>(&mut self, shelf_id: SId, mut media_ids: Vec<SMId>) -> Result<()>
    where SId: AsRef<str>, SMId: Into<String> {
        // TODO:DATABASE: In the future shelf:book relationships will
        // likely be stored in a separate table with order information
        let mut shelf = self.shelves.get_mut(shelf_id.as_ref())
            .context(ShelfNotFound {id: shelf_id.as_ref()})?;
        shelf.books = media_ids.drain(..)
            .map(|m| m.into())
            .collect();

        Ok(())
    }

    pub fn set_shelf_subshelves<SId, SSId>(&mut self, shelf_id: SId, mut shelf_ids: Vec<SSId>) -> Result<()>
    where SId: AsRef<str>, SSId: Into<String> {
        let mut shelf = self.shelves.get_mut(shelf_id.as_ref())
            .context(ShelfNotFound {id: shelf_id.as_ref()})?;
        shelf.subshelves = shelf_ids.drain(..)
            .map(|s| s.into())
            .collect();

        Ok(())
    }
}

/// Global information about database
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DatabaseInfo {
    pub database_version: usize,
    pub id_type_media: String,
    pub id_type_shelf: String,
}

/// Books are grouped into shelves, struct contains shelf metadata and book list
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Shelf {
    /// List of IDs of Books contained in this shelf
    pub books: Vec<String>,
    /// ID of this shelf
    pub id: String,
    /// Name of this shelf
    pub name: String,
    /// Path of the shelf directory on disk
    pub path: PathBuf,
    /// List of IDs of Shelves contained in this shelf
    pub subshelves: Vec<String>,
}

impl Shelf {
    /// Construct an empty shelf
    pub fn new<SId, SName, P>(id: SId, name: SName, path: P) -> Self
    where SId: Into<String>, SName: Into<String>, P: Into<PathBuf> {
        Self {
            books: Vec::new(),
            id: id.into(),
            name: name.into(),
            path: path.into(),
            subshelves: Vec::new(),
        }
    }
}

/// Information about a book
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Book {
    /// ID for this book
    pub id: String,
    /// Title of book
    pub title: String,
    /// Date when book record was first created
    pub date_added: DateTime<Utc>,
    /// Date of last modification of this record, used when updating during scan
    pub date_modified: DateTime<Utc>,
    /// URL book can be read from
    pub url: String,
    /// List of URLs for book pages (bare directories only, None otherwise)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub page_urls: Option<Vec<String>>,
    /// ID of previous book or None if first
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prev_id: Option<String>,
    /// ID of next book or None if last
    #[serde(skip_serializing_if = "Option::is_none")]
    pub next_id: Option<String>,
    /// File path of the source book
    pub path: PathBuf,
}

impl Book {
    /// Create new Book. Consumes metadata.
    pub fn new<SId, SUrl, P>(id: SId, meta: BookMetadata, url: SUrl, path: P) -> Self
    where SId: Into<String>, SUrl: Into<String>, P: Into<PathBuf> {
        Self {
            id: id.into(),
            title: meta.title,
            // TODO: Properly track timestamps. date_added is reset on modification and
            // date_modified is same as date_added.
            date_added: meta.date_added,
            date_modified: meta.date_added,
            url: url.into(),
            page_urls: None,
            prev_id: None,
            next_id: None,
            path: path.into(),
        }
    }
}


// Tests found in tests/media_database.rs
