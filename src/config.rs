use std::{fs, collections::{BTreeMap, HashMap, HashSet}, path::{Path, PathBuf}};
use chrono::{DateTime, Duration, Utc};
use clap::ArgMatches;
use path_abs::PathAbs;
use serde::{Deserialize, Serialize};
use snafu::{OptionExt, ResultExt, Snafu, ensure};

use doccommentreader::DocCommentReader;
use crate::util::{encode_path_as_url, remove_trailing_slash};
use crate::ui::{UserInterface, UserInterfaceNull};


/// Used in configuration file generation to indicate fields that have defaults
/// derived from other fields or different behavior when not specified rather
/// than simply filling in a default.
const CONFIG_FIELD_UNSET: &str = "(Unset)";
const CONFIG_FIELD_REQUIRED: &str = "Required to be set";
const DEFAULT_METADATA_PATH: &str = "metadata";
const DEFAULT_BROOKREADER_DATA_PATH: &str = "data";
const DEFAULT_BUILD_OUTPUT_PATH: &str = "build";
const DEFAULT_DATABASE_LOCATION: &str = "database.json";
const DEFAULT_SHOWCASE_RECENT_DAYS_AGO: i64 = 14;
const VALID_MEDIA_ID_TYPES: &[&str] = &["content", "filename", "path"];
const VALID_SHELF_ID_TYPES: &[&str] = &["filename", "path"];
const VALID_SHOWCASE_SECTIONS: &[&str] = &["featured", "recent"];

/// Error Snafu enum internal to the module. Exported functions will return a
/// wrapper newtype.
#[derive(Debug, Snafu)]
enum InternalError {
    #[snafu(display("Could not determine cache directory"))]
    CacheDirUnknown {},
    #[snafu(display("Cannot deserialize configuration file '{}': {}", path.display(), source))]
    ConfigFileDeserialize { path: PathBuf, source: toml::de::Error },
    #[snafu(display("Cannot read configuration file '{}': {}", path.display(), source))]
    ConfigFileRead { path: PathBuf, source: std::io::Error },
    #[snafu(display("Cannot serialize configuration: {}", source))]
    ConfigFileSerialize { source: toml::ser::Error },
    #[snafu(display("Cannot write configuration file '{}': {}", path.display(), source))]
    ConfigFileWrite { path: PathBuf, source: std::io::Error },
    #[snafu(display("Configuration invalid: {}", reason))]
    ConfigInvalid { reason: String },
    #[snafu(display("Config path '{}' is not a valid file", path.display() ))]
    InvalidConfigPath { path: PathBuf },
    #[snafu(display("Path '{}' is not a valid file or directory: {}", path.display(), source))]
    InvalidPath { path: PathBuf, source: std::io::Error },
    #[snafu(display("Path '{}' is not valid: {}", path.display(), source))]
    InvalidPathError { path: PathBuf, source: path_abs::Error },
    #[snafu(display("Configured path '{}' for setting {} is not a valid file or directory", path.display(), setting_name))]
    InvalidConfiguredPath { path: PathBuf, setting_name: String},
}
type InternalResult<T, E = InternalError> = std::result::Result<T, E>;
/// Error struct for configuration problems
#[derive(Debug, Snafu)]
pub struct Error(InternalError);
type Result<T> = InternalResult<T, Error>;

/** Contains the current configuration of the program. Includes internal
configuration as well, such as callback hooks.
*/
#[derive(Debug)]
pub struct Config {
    /// Root working directory
    pub base_path: PathBuf,
    /// Maximum of cover thumbnails in pixels
    pub cover_thumbnail_width: usize,
    /// Maximum of cover thumbnails in pixels
    pub cover_thumbnail_height: usize,
    /// Image format for covers
    pub cover_format: String,
    /// Image format for cover thumbnails
    pub cover_thumb_format: String,
    /// Path to unrar executable
    pub unrar_executable: PathBuf,

    /// Base directory for frontend data
    pub brookreader_data_path: PathBuf,
    /// Base URL frontend data is served from
    pub brookreader_data_url: String,
    /// Directory containing cover images to serve
    pub build_cover_path: PathBuf,
    /// Contents of this directory will be added to build if set
    pub build_frontend_path: Option<PathBuf>,
    /// Base directory for generated output
    pub build_output_path: PathBuf,
    /// Contents of this directory will be added to build if set
    pub build_static_path: Option<PathBuf>,
    /// Create symlinks to static dir contents instead of copying
    pub build_static_symlink: bool,
    /// Where to find the media database
    pub database_location: PathBuf,
    /// Path to write log
    pub log_path: PathBuf,
    /// Base directory for books
    pub media_books_root: PathBuf,
    /// Base URL books are served from
    pub media_books_url: String,
    /// Algorithm for generating IDs for content
    pub scan_id_type_media: String,
    /// Algorithm for generating IDs for shelves
    pub scan_id_type_shelf: String,
    /// Base directory for metadata files
    pub scan_metadata_path: PathBuf,
    /// Maximum length of IDs generated from filenames
    pub scan_filename_id_max_length: usize,
    /// Date cutoff for recently added items
    pub showcase_recent_threshold: DateTime<Utc>,
    /// Names of showcases to output
    pub showcase_sections: Vec<String>,

    /// User interface callback hooks
    pub ui_hooks: Box<dyn UserInterface>,
}

impl Config {
    /// Load configuration from file
    pub fn load(config_path: &Path, cli_matches: &ArgMatches) -> Result<Config> {
        let config_text = fs::read_to_string(config_path)
            .context(ConfigFileRead {path: config_path})?;
        let config_file: ConfigFile = toml::from_str(&config_text)
            .context(ConfigFileDeserialize {path: config_path})?;

        // Base values used by other settings
        let defaults = Self::default();
        let config_path_abs = abs_path(config_path)?;
        let base_path = config_path_abs.parent()
            .context(InvalidConfigPath {path: config_path})?
            .to_owned();
        let build_output_path = config_file.build_output_path
            .map(|p| base_path.join(p))
            .unwrap_or(base_path.join(defaults.build_output_path));
        // TODO: Why difference in names?
        let relative_data_path = config_file.brookreader_data_dir
            .unwrap_or(defaults.brookreader_data_path);
        let brookreader_data_path = build_output_path.join(&relative_data_path);

        // Set configuration from config file or defaults.
        // Note that paths from config file may be relative, they will be
        // joined to the base path to become absolute.

        let brookreader_data_url = config_file.brookreader_data_url
            .or_else(|| Some(encode_path_as_url(&relative_data_path)) )
            .unwrap_or(defaults.brookreader_data_url);

        let build_cover_path = brookreader_data_path.join("covers");
        let build_frontend_path = config_file.build_frontend_path
            .map(|p| base_path.join(p) );
        let build_static_path = config_file.build_static_path
            .map(|p| base_path.join(p) );
        let build_static_symlink = config_file.build_static_symlink
            .unwrap_or(defaults.build_static_symlink);

        let database_location = config_file.database_location
            .map(|p| base_path.join(p))
            .unwrap_or(base_path.join(defaults.database_location));

        let log_path = config_file.log_path
            .map(|p| base_path.join(p))
            .unwrap_or(defaults.log_path);

        let media_books_root = base_path.join(&config_file.media_books_root);
        let media_books_url = remove_trailing_slash(config_file.media_books_url);

        let scan_id_type_media = config_file.scan_id_type_media
            .unwrap_or(defaults.scan_id_type_media);
        let scan_id_type_shelf = config_file.scan_id_type_shelf
            .unwrap_or(defaults.scan_id_type_shelf);
        let scan_metadata_path = base_path.join(DEFAULT_METADATA_PATH);

        let showcase_recent_threshold = config_file.showcase_recent_days_ago
            .map(|r| Utc::now() - Duration::days(r as i64) )
            .unwrap_or(defaults.showcase_recent_threshold);
        let showcase_sections = config_file.showcase_sections
            .unwrap_or(defaults.showcase_sections);

        let mut config = Config {
            base_path,

            cover_thumbnail_width: defaults.cover_thumbnail_width,
            cover_thumbnail_height: defaults.cover_thumbnail_height,
            cover_format: defaults.cover_format,
            cover_thumb_format: defaults.cover_thumb_format,
            scan_filename_id_max_length: defaults.scan_filename_id_max_length,
            unrar_executable: defaults.unrar_executable,

            brookreader_data_path,
            brookreader_data_url,
            build_cover_path,
            build_frontend_path,
            build_output_path,
            build_static_path,
            build_static_symlink,
            database_location,
            log_path,
            media_books_root,
            media_books_url,
            scan_id_type_media,
            scan_id_type_shelf,
            scan_metadata_path,
            showcase_recent_threshold,
            showcase_sections,

            ui_hooks: defaults.ui_hooks,
        };

        config.merge_cli_args(cli_matches);

        config.validate()?;

        // Unrar setup
        crate::archive::set_unrar_path(&config.unrar_executable);

        Ok(config)
    }

    /// Assign UI hooks. Cannot be done at creation because hooks may depend on
    /// configuration settings.
    pub fn set_ui_hooks(&mut self, hooks: impl UserInterface + 'static) {
        self.ui_hooks = Box::new(hooks)
    }

    /// Set CLI config overrides
    fn merge_cli_args(&mut self, _cli_matches: &ArgMatches) {
        // Currently none but there will be in the future
    }

    /// Sanity check of configuration values
    fn validate(&self) -> InternalResult<()> {
        let data_path_clean = abs_path(&self.brookreader_data_path)?;
        ensure!(
            data_path_clean.starts_with(&self.build_output_path),
            ConfigInvalid {reason: "brookreader_data_dir must be subdirectory of build_output_path"}
        );
        ensure!(
            VALID_MEDIA_ID_TYPES.contains(&self.scan_id_type_media.as_str()),
            ConfigInvalid {reason: "scan_media_id_type must be one of 'content', 'filename', or 'path'"}
        );
        ensure!(
            VALID_SHELF_ID_TYPES.contains(&self.scan_id_type_shelf.as_str()),
            ConfigInvalid {reason: "scan_shelf_id_type must be one of 'filename' or 'path'"}
        );
        for section in self.showcase_sections.iter() {
            ensure!(
                VALID_SHOWCASE_SECTIONS.contains(&section.as_str()),
                ConfigInvalid {reason: format!(
                    "showcase_sections contains invalid section '{}'",
                    section
                )}
            );
        }

        std::process::Command::new(&self.unrar_executable).output()
            .map_err(|e| InternalError::ConfigInvalid {reason: format!(
                "unrar_executable path '{}' not a valid program: {}",
                &self.unrar_executable.display(),
                e
            )})?;

        Ok(())
    }
}

impl Default for Config {
    /// Default configuration values. Note that paths are relative and should
    /// be joined to a base path.
    fn default() -> Self {
        let brookreader_data_base_path = Path::new(DEFAULT_BROOKREADER_DATA_PATH);
        Self {
            base_path: PathBuf::from("CONTAINING DIRECTORY OF CONFIG FILE"),
            cover_thumbnail_width: 200,
            cover_thumbnail_height: 300,
            cover_format: "jpg".to_owned(),
            cover_thumb_format: "png".to_owned(),
            unrar_executable: PathBuf::from("unrar"),

            brookreader_data_path: brookreader_data_base_path.to_owned(),
            brookreader_data_url: encode_path_as_url(DEFAULT_BROOKREADER_DATA_PATH),
            build_cover_path: brookreader_data_base_path.join("covers"),
            build_frontend_path: None,
            build_output_path: PathBuf::from(DEFAULT_BUILD_OUTPUT_PATH),
            build_static_path: None,
            build_static_symlink: false,
            database_location: PathBuf::from(DEFAULT_DATABASE_LOCATION),
            log_path: PathBuf::from("log.txt"),
            // TODO: Check usage of default(). maybe new(<required opts>) would be better?
            media_books_root: PathBuf::from("REQUIRED TO BE SET"),
            media_books_url: String::from("REQUIRED TO BE SET"),
            scan_filename_id_max_length: 64,
            scan_id_type_media: String::from("path"),
            scan_id_type_shelf: String::from("path"),
            scan_metadata_path: PathBuf::new(),
            showcase_recent_threshold: Utc::now() - Duration::days(DEFAULT_SHOWCASE_RECENT_DAYS_AGO),
            showcase_sections: vec!["featured".to_owned(), "recent".to_owned()],

            ui_hooks: Box::new(UserInterfaceNull::default()),
        }
    }
}


/// Canonicalize a path without resolving symlinks. Uses PathAbs internally.
fn abs_path(path: &Path) -> InternalResult<PathBuf> {
    let canon = PathAbs::new(path)
        .context(InvalidPathError {path})?
        .as_path()
        .to_owned();
    Ok(canon)
}


/** Configuration file for a Brookreader deployment

All paths except where noted may be absolute or relative to the location of the
configuration file.
*/
#[derive(Debug, DocCommentReader, Deserialize, Serialize)]
pub struct ConfigFile {
    /// Base directory to build frontend data files (media index/details files,
    /// covers, etc.). Path is relative to build_output_path and must be a
    /// subdirectory of it.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub brookreader_data_dir: Option<PathBuf>,
    /// Base URL where files in brookreader_data_path are served. If not
    /// specified URL is derived from path.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub brookreader_data_url: Option<String>,

    /// Directory containing built frontend files. If set copies contents of
    /// target directory into the build output directory, otherwise no action
    /// is taken.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub build_frontend_path: Option<PathBuf>,
    /// Directory to write final output files, including frontend data,
    /// frontend/static files, etc.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub build_output_path: Option<PathBuf>,
    /// Directory containing static frontend files. If set copies contents of
    /// target directory into the build output directory, otherwise no action
    /// is taken.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub build_static_path: Option<PathBuf>,
    /// When true symlinks contents of static directory during build
    /// process, otherwise copies directory contents.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub build_static_symlink: Option<bool>,

    /// Path of media database.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub database_location: Option<PathBuf>,

    /// Path to write log file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub log_path: Option<PathBuf>,

    /// Root directory where book files are found.
    pub media_books_root: PathBuf,
    /// Base URL for books.
    pub media_books_url: String,

    /// ID generation method to use for media. Can be one of the
    /// following values:
    ///  "path" - ID is based on a hash of the item's file path
    ///  "content" - ID is based on a hash of the item's file contents
    ///  "filename" - ID is based on the item's filename, with
    ///     punctuation and spaces removed
    /// See the documentation for advantages and disadvantages of each
    /// ID type.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub scan_id_type_media: Option<String>,
    /// ID generation method to use for shelves. Can be one of the
    /// following values:
    ///  "path" - ID is based on a hash of the shelf's file path
    ///  "filename" - ID is based on the shelf's filename, with
    ///     punctuation and spaces removed
    /// See the documentation for advantages and disadvantages of each
    /// ID type.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub scan_id_type_shelf: Option<String>,

    /// Anything added this many or less days ago will appear on the recently
    /// added showcase.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub showcase_recent_days_ago: Option<usize>,
    /// List of sections to display on showcase. Order of sections in output is
    /// the same as order listed in config.Valid sections are "featured"  and
    /// "recent".
    ///
    /// TODO:FEBUILD: Section ordering is handled by FE, set FE's order from this
    #[serde(skip_serializing_if = "Option::is_none")]
    pub showcase_sections: Option<Vec<String>>,
}

impl ConfigFile {
    /// Create a new configuration file from this struct's fields
    ///
    /// Rather than simply serializing a ConfigFile struct, this function also
    /// includes the documentation comments for each field. It will also write the
    /// ConfigFile struct's documentation comment at the head of the file.
    pub fn write_annotated_config(&self, path: &Path) -> Result<()> {
        let fields_comments = ConfigFile::get_doc_comments().into_iter()
            .map(|(n, c)| {
                let comment = ConfigFile::commentize_str(c);
                (n, comment)
            })
            .collect::<HashMap<&str, String>>();

        let default_config = ConfigFile::default();
        let default_serialized = default_config.to_serialized_lines();

        let unset_quoted = format!(r#""{}""#, CONFIG_FIELD_UNSET);
        let required_quoted = format!(r#""{}""#, CONFIG_FIELD_REQUIRED);
        let new_serialized = self.to_serialized_lines();
        let mut seen_fields = HashSet::new();
        let mut new_config_toml = ConfigFile::commentize_str(ConfigFile::get_type_doc_comment());
        new_config_toml.push_str("\n\n\n");
        for (field_name, field_line) in new_serialized.iter() {
            seen_fields.insert(field_name.as_str());
            // Check that required field was set
            let field_setting = field_line
                .split(" = ").skip(1).next()
                .expect("All config fields should have a value before processing");
            if field_setting == required_quoted {
                panic!("Required ConfigFile field {} was not set", &field_name);
            }
            let field_comment = fields_comments.get(field_name.as_str())
                .expect("All config fields must have a doc comment");
            let field_default = default_serialized.get(field_name)
                .expect("All config fields must be present in default")
                .split(" = ").skip(1).next()
                .expect("All default serialzed config fields should have a value");
            let field_default_unset = field_default == unset_quoted;
            // Text to use in comment default when default is unset
            let field_default_comment_value = match field_default_unset {
                true => CONFIG_FIELD_UNSET,
                false => &field_default
            };
            // Use different text for default note field is required
            let field_default_comment = match field_default == required_quoted {
                true => CONFIG_FIELD_REQUIRED.to_owned(),
                false => format!("Default: {}", field_default_comment_value)
            };
            // Trim unset text from field line if present
            let field_output_line = match field_setting == unset_quoted {
                true => &field_line[..field_line.len() - (CONFIG_FIELD_UNSET.len() + 2)],
                false => field_line.as_str()
            };
            // UNSTABLE: Option::contains()
            // Prefix setting with a TOML comment (#) when using default/unset value
            let field_overridden = Some(field_line) != default_serialized.get(field_name);
            let field_prefix_commented = if field_overridden {""} else {"#"};

            let field_output = format!("{}\n#\n# {}\n{}{}\n\n",
                field_comment,
                field_default_comment,
                field_prefix_commented,
                field_output_line,
            );
            new_config_toml.push_str(&field_output);
        }

        // Check that no fields are missing
        let all_fields = fields_comments.keys().map(|k| *k).collect::<HashSet<_>>();
        let mut missing_fields = all_fields.difference(&seen_fields)
            .map(|k| *k)
            .collect::<Vec<_>>();
        missing_fields.sort_unstable();
        if seen_fields != all_fields {
            // Panic instead of error - if this happens at runtime fix your missing fields
            panic!("Required configuration fields were not set: {}", missing_fields.join(", "));
        }

        fs::write(path, new_config_toml).context(ConfigFileWrite {path})?;
        Ok(())
    }

    /// Prepend each line in a string with TOML comments
    fn commentize_str(input: &str) -> String {
        let joined = input.split('\n')
            .collect::<Vec<_>>()
            .join("\n# ");
        let mut comment = String::from("# ");
        comment.push_str(&joined);
        comment
    }

    /// Converts a ConfigFile struct into a hashmap pairing each field name
    /// with the serialized representation of the field
    ///
    /// Returns a BTreeMap to keep keys sorted.
    fn to_serialized_lines(&self) -> BTreeMap<String, String> {
        let serialized = toml::to_string(self)
            .expect("ConfigFile should be serializable");
        serialized.split('\n')
            .filter_map(|line| {
                // Blank lines are skipped
                line.split_ascii_whitespace().next()
                    .map(|f| (f.to_owned(), line.to_owned()))
            })
            .collect::<BTreeMap<String, String>>()
    }
}

impl Default for ConfigFile {
    /// Create a struct with default values
    ///
    /// Generated config files will use values here as default. Set a field to
    /// CONFIG_FIELD_UNSET for (unset) fields.
    fn default() -> Self {
        // TODO: Instead of CONFIG_FIELD_UNSET, set to None and create a new() method?
        // Or possibly just set to None and require calling code to set fields?
        let default_showcase_sections = vec!["featured".to_owned(), "recent".to_owned()];
        Self {
            brookreader_data_dir: Some(PathBuf::from("data")),
            brookreader_data_url: Some(String::from(CONFIG_FIELD_UNSET)),
            build_frontend_path: Some(PathBuf::from(CONFIG_FIELD_UNSET)),
            build_output_path: Some(PathBuf::from("build")),
            build_static_path: Some(PathBuf::from(CONFIG_FIELD_UNSET)),
            build_static_symlink: Some(false),
            database_location: Some(PathBuf::from("database.json")),
            log_path: Some(PathBuf::from("log.txt")),
            media_books_root: PathBuf::from(CONFIG_FIELD_REQUIRED),
            media_books_url: String::from(CONFIG_FIELD_REQUIRED),
            scan_id_type_media: Some(String::from("path")),
            scan_id_type_shelf: Some(String::from("path")),
            showcase_recent_days_ago: Some(14),
            showcase_sections: Some(default_showcase_sections),
        }
    }
}


#[cfg(test)]
#[path = "./config.test.rs"]
mod config_test;
