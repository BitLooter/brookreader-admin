// TODO: Check invisible not scanned because of .ignore
// TODO: order.txt not treated as book (after txt files recognized as books)
// TODO: Other files not scanned as books (e.g. test-notbook.exe)
// TODO: Nested directories scanned as shelves

use crate::tests_common;
use crate::mediadb;
// use crate::ui::{self, UiAction, UserInterface};


// #[derive(Debug)]
// struct UserInterfaceMonitor {}

// // impl<'counter> UserInterfaceMonitor<'counter> {
// impl UserInterfaceMonitor {
//     fn new() -> Self {
//         Self {}
//     }
// }

// impl UserInterface for UserInterfaceMonitor {
//     fn scan_media_item_end(&self, _scan_path: &Path, _media_path: &Path, _media_id: &str, _scan_action: UiAction) -> ui::Result<()> {
//         Ok(())
//     }
// }

#[test]
fn scan_books() {
    let config = tests_common::testenv_config();

    // TODO: Should scan return anything? Maybe a MediaDatabase?
    super::scan(&config)
        .expect("Error scanning test library");

    // TODO: Track during scanning using ui_hooks when ExecutionEnvironment implemented
    //  instead of querying the database
    let db = mediadb::MediaDatabase::open(&config.database_location)
        .expect("Could not open test instance database");
    // TODO: More extensive tests when above is done
    assert_eq!(tests_common::TOTAL_BOOKS, db.num_books());
}
