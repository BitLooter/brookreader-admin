use crate::tests_common;


fn make_discoverer() -> super::MediaDiscoverer {
    let config = tests_common::testenv_config();
    super::MediaDiscoverer::new(&config)
}

#[test]
fn find_book_files() {
    let discoverer = make_discoverer();

    let books = discoverer.all_books()
        .collect::<Vec<_>>();
    // TODO: More extensive tests of what is and isn't found
    assert_eq!(16, books.len());
}

#[test]
fn find_shelf_directories() {
    let discoverer = make_discoverer();

    let shelves = discoverer.all_book_shelves()
        .collect::<Vec<_>>();
    assert_eq!(5, shelves.len())
}
