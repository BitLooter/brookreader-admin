use std::{
    fmt,
    io,
    path::{Path, PathBuf}
};
use walkdir::WalkDir;

use crate::config::Config;
use crate::util::{path_is_book_file, path_is_image};


#[derive(Debug)]
pub struct Error {
    message: String,
}
impl Error {
    fn new<S: Into<String>>(message: S) -> Self {
        Self {
            message: message.into(),
        }
    }
}
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", &self.message)
    }
}
impl std::error::Error for Error {}

/// Manages searching for media, providing methods returning iterators over paths
pub struct MediaDiscoverer {
    pub root_path: PathBuf,
}

impl MediaDiscoverer {
    pub fn new(config: &Config) -> Self {
        return Self {
            root_path: config.media_books_root.clone(),
        }
    }

    /// Returns an iterator over all paths matching books under the root path
    pub fn all_books(&self) -> impl Iterator<Item = Result<PathBuf, Error>> + '_ {
        self.walk_filter(move |p| self.path_is_book(p))
    }

    /// Returns an iterator over all paths containing books under the root path
    pub fn all_book_shelves(&self) -> impl Iterator<Item = Result<PathBuf, Error>> + '_ {
        self.walk_filter(move |p| self.path_is_book_shelf(p))
    }

    /// Returns iterator over all files in the root path. Skips directories
    /// containing an '.ignore' file. Not used directly by outside code,
    /// results should be filtered through one of the search methods.
    fn walk(&self) -> impl Iterator<Item = Result<PathBuf, walkdir::Error>> {
        WalkDir::new(&self.root_path)
            .min_depth(1)
            .follow_links(true)
            .into_iter()
            .filter_entry(|e|
                // Ignore any directories that contain an '.ignore' file
                !e.path().join(".ignore").exists()
            )
            .map(|e| e.map(|e_ok| e_ok.into_path()) )
    }

    /// Returns an iterator over all paths matching books under the root path
    pub fn walk_filter<'a, F>(&self, filter_func: F) -> impl Iterator<Item = Result<PathBuf, Error>> + 'a
    where F: 'a + Fn(&Path) -> Result<bool, io::Error>
    {
        self.walk()
            .filter_map(move |p| {
                // If p is an error, translate it into the Error struct.
                // Otherwise check path against filter_func and if that
                // function returns an error, return an Error. If no errors
                // were encountered, return Some (with a Result::Ok) or None
                // depending on if path represents a shelf.
                match p {
                    Ok(entry_path) => {
                        match filter_func(&entry_path) {
                            Ok(passes_filter) => match passes_filter {
                                // UNSTABLE: bool.then() https://github.com/rust-lang/rust/issues/64260
                                true => Some(Ok(entry_path)),
                                false => None
                            }
                            Err(path_check_error) => Some(Err(Error::new(
                                format!(
                                    "Could not determine if {} passes filter: {}",
                                    entry_path.display(),
                                    path_check_error
                                )
                            )))
                        }
                    },
                    Err(walk_error) => Some(Err(Error::new(
                        format!("Error walking directory tree: {}", walk_error)
                    )))
                }
            })
    }

    // Path type checkers

    /// Determines if given path represents a book, including bare directories
    fn path_is_book(&self, path: &Path) -> Result<bool, io::Error> {
        Ok(path_is_book_file(path) || self.path_is_book_baredir(path)?)
    }

    /// Determines if given path represents a bare directory book. A bare
    /// directory book must contain at least one image and no subdirectories or
    /// other books.
    fn path_is_book_baredir(&self, path: &Path) -> Result<bool, io::Error> {
        if !path.is_dir() {
            return Ok(false)
        }

        let mut image_found = false;

        for dir_path in path.read_dir()? {
            let dir_path = dir_path?.path();
            if dir_path.is_dir() || path_is_book_file(&dir_path) {
                return Ok(false)
            }
            if path_is_image(&dir_path) { image_found = true }
        }

        return Ok(image_found);
    }

    /// Determins if path represents a shelf containing books
    fn path_is_book_shelf(&self, path: &Path) -> Result<bool, io::Error> {
        if !path.is_dir() { return Ok(false) }
        if path.join(".ignore").exists() {
            // Ignored directories not walked but can still get here through
            // the recursive check below
            return Ok(false)
        }
        for entry in path.read_dir()? {
            let entry_path = entry?.path();
            if self.path_is_book(&entry_path)? || self.path_is_book_shelf(&entry_path)? {
                return Ok(true)
            }
        }
        // No books found, not a book shelf
        Ok(false)
    }
}


#[cfg(test)]
#[path = "./discovery.test.rs"]
mod discovery_test;
