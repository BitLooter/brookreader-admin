use crate::tests_common;


#[test]
fn generate_filename_ids() {
    let config = tests_common::testenv_config();
    let book_path = tests_common::testenv_get_book_path("test-cbz");
    let res = super::ResourceId::new(&config)
        .expect("Could not create ResourceId struct");

    let book_id = res.create_book_id(&book_path)
        .expect("Could not generate ID");
    assert_eq!("test-cbz", &book_id);
    // TODO: Establish exactly how filenames are turned into IDs and test it
}

#[test]
fn generate_path_ids() {
    let mut config = tests_common::testenv_config();
    config.scan_id_type_media = "path".into();
    config.scan_id_type_shelf = "path".into();
    let book_path = tests_common::testenv_get_book_path("test-cbz");
    let res = super::ResourceId::new(&config)
        .expect("Could not create ResourceId struct");

    let id = res.create_book_id(&book_path)
        .expect("Could not generate ID");

    assert_eq!("6495103f6ee20194", &id);
    // TODO: Check unusual paths
}

#[test]
fn generate_content_ids() {
    let mut config = tests_common::testenv_config();
    config.scan_id_type_media = "content".into();
    let book_path = tests_common::testenv_get_book_path("test-cbz");
    let res = super::ResourceId::new(&config)
        .expect("Could not create ResourceId struct");

    let id = res.create_book_id(&book_path)
        .expect("Could not generate ID");

    assert_eq!("18f0a0e61c0c6408", &id);
    // TODO: Test hashing every content type
}
