use std::{ hash::{self, Hasher}, io };
use twox_hash::XxHash64;


// Arbitrarily chosen as the Unix timestamp as of writing this
const XXHASH_SEED: u64 = 1575248759;

/// Newtype used to add Write trait to Hashers, useful for io::copy()
#[derive(Debug)]
pub struct HasherWriter<T: Hasher>(T);

impl<T: Hasher> HasherWriter<T> {
    fn write(&mut self, buffer: &[u8]) {
        Hasher::write(self, buffer)
    }
}

impl<T: Hasher> hash::Hasher for HasherWriter<T> {
    fn finish(&self) -> u64 {
        self.0.finish()
    }

    fn write(&mut self, buffer: &[u8]) {
        self.0.write(buffer)
    }
}

impl<T: Hasher> io::Write for HasherWriter<T> {
    fn write(&mut self, buffer: &[u8]) -> io::Result<usize> {
        self.0.write(buffer);
        Ok(buffer.len())
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

/// TODO: Why is this public? Why not a hash_buffer_xxhash64() wrapper function?
pub fn create_hasher_xxhash64() -> HasherWriter<XxHash64> {
    HasherWriter(XxHash64::with_seed(XXHASH_SEED))
}

/// Calculates the XxHash64 of the buffer
pub fn hash_xxhash64<B: AsRef<[u8]>>(buffer: B) -> u64 {
    let mut hasher = create_hasher_xxhash64();
    hasher.write(buffer.as_ref());
    hasher.finish()
}


#[cfg(test)]
#[path = "./hash.test.rs"]
mod hash_test;
