#[test]
fn xxhash_smoke_test() {
    let hash1 = super::hash_xxhash64("Test string 1");
    assert_eq!(7232534816128421377, hash1);
    let hash2 = super::hash_xxhash64("Test string 2");
    assert_eq!(14627731941928761817, hash2);
    // TODO: Test hashing buffers when hash_buffer_xxhash64() implemented
}
