use std::{
    collections::{HashMap, HashSet},
    fs::File,
    io::{self, BufRead, BufReader},
    path::{Path, PathBuf},
};
use chrono::{DateTime, Utc};
use snafu::{ResultExt, Snafu};


use crate::Config;
use crate::mediadb::{self, Book, MediaDatabase};
use crate::metadata::{self, MetadataManager};
use crate::ui::{self, UiAction, UiMediaType};
use crate::util::{encode_path_as_url, path_is_image};
use super::discovery::{self, MediaDiscoverer};
use super::resourceid::{self, ResourceId};


#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Problem with database: {}", source))]
    DatabaseOperationError { source: mediadb::Error },
    #[snafu(display("Could not remove shelf {} from database: {}", shelf_id, source))]
    DatabaseShelfRemoveError { shelf_id: String, source: mediadb::Error },
    #[snafu(display("Could not remove {} from database: {}", media_id, source))]
    DatabaseRemoveError { media_id: String, source: mediadb::Error },
    #[snafu(display("Could not read directory {}: {}", path.display(), source))]
    DirectoryRead { path: PathBuf, source: io::Error },
    #[snafu(display("Duplicate ID found, {} has same ID as {} ({})", orig_path.display(), dupe_path.display(), id))]
    DuplicateIdError { orig_path: PathBuf, dupe_path: PathBuf, id: String },
    #[snafu(display("ID generator problem: {}", source))]
    ResourceIdError { source: resourceid::Error },
    #[snafu(display("Could not load metadata for file {} ({}): {}", path.display(), id, source))]
    LoadMetadata { path: PathBuf, id: String, source: metadata::Error },
    #[snafu(display("Could not read from path {}: {}", path.display(), source))]
    PathRead { path: PathBuf, source: io::Error },
    #[snafu(display("Error searching for media: {}", source))]
    DiscoveryError { source: discovery::Error },
    #[snafu(display("Ui error: {}", source))]
    UiError { source: ui::Error },
}

impl From<ui::Error> for Error {
    fn from(source: ui::Error) -> Self {
        Self::UiError {source}
    }
}

type Result<T, E = Error> = std::result::Result<T, E>;
type PathIdMap = HashMap<PathBuf, String>;

/// Scan for books and output indexes and metadata, based on given config
pub fn scan(config: &Config) -> Result<()> {
    let mut database = MediaDatabase::open(&config.database_location)
        // TODO:DATABASE: Don't silently fail on bad database
        .or( Ok(MediaDatabase::new(&config.scan_id_type_media, &config.scan_id_type_shelf)) )
        .context( DatabaseOperationError {} )?;
    let discoverer = MediaDiscoverer::new(&config);
    let id_gen = ResourceId::new(&config)
        .context( ResourceIdError {} )?;

    config.ui_hooks.scan_begin(&config.media_books_root)?;

    // Information gathering
    let shelf_id_path_map = scan_all_shelves(&discoverer, &mut database, &id_gen, config)?;
    let book_id_path_map = scan_all_books(&discoverer, &mut database, &id_gen, config)?;

    // Scan data postprocessing
    config.ui_hooks.scan_postprocessing(&config.media_books_root)?;
    prune_database(&mut database, &shelf_id_path_map, &book_id_path_map, &config)?;
    link_database_entries(&mut database, &shelf_id_path_map, &book_id_path_map, &config.media_books_root)?;

    // All done, output data
    database.write_json(&config.database_location)
        .context( DatabaseOperationError )?;

    config.ui_hooks.scan_end(&config.media_books_root)?;

    Ok(())
}

/// Create shelves based on directory structure. Returns vec of all shelf paths
/// scanned.
fn scan_all_shelves(
    discoverer: &MediaDiscoverer,
    database: &mut MediaDatabase,
    id_gen: &ResourceId,
    config: &Config
) -> Result<PathIdMap>
{
    config.ui_hooks.scan_shelf_discovery_begin(&config.media_books_root)?;

    let mut shelf_path_id_map = PathIdMap::new();
    database.create_shelf("Main shelf", mediadb::MAIN_SHELF_ID, Path::new(""), None)
        .expect("Should not fail with None as parent");
    shelf_path_id_map.insert(PathBuf::from(""), mediadb::MAIN_SHELF_ID.to_string());
    for shelf_dir in discoverer.all_book_shelves() {
        let shelf_relative_path = shelf_dir
            .context(DiscoveryError {})?
            .strip_prefix(&discoverer.root_path)
            .expect("Shelf path not in books root")
            .to_path_buf();
        // Safe to unwrap, path is relative and will be "" if no parent
        let parent_shelf_dir = shelf_relative_path
            .parent()
            .expect(&format!("Can't get parent path of {}", shelf_relative_path.display()))
            .to_path_buf();
        // Safe to unwrap, path will contain at least one component
        let shelf_name = shelf_relative_path
            .file_name()
            .expect(&format!("Can't get final directory name of {}", &shelf_relative_path.display()))
            .to_string_lossy();
        // Safe to unwrap, parent_shelf_dir was added in previous loop iteration
        let parent_id = shelf_path_id_map.get(&parent_shelf_dir)
            .expect(&format!("Unknown parent shelf ID {}", &parent_shelf_dir.display()));
        let new_shelf_id = id_gen.create_shelf_id(&shelf_relative_path)
            .expect("Shelf ID generation should not fail, uses Result for consistancy");

        // Check this shelf's ID not a dupe, most likely when using filename IDs
        if let Some((orig_path, _orig_id)) = shelf_path_id_map.iter().find(|(_, v)| **v == new_shelf_id) {
            Err(Error::DuplicateIdError {
                orig_path: orig_path.to_path_buf(),
                dupe_path: shelf_relative_path.to_path_buf(),
                id: new_shelf_id.clone()
            })?;
        }
        database.create_shelf(&shelf_name, &new_shelf_id, &shelf_relative_path, Some(parent_id))
            .expect("MAIN_SHELF_ID should be in database");
        shelf_path_id_map.insert(shelf_relative_path, new_shelf_id);
    }

    config.ui_hooks.scan_shelf_discovery_end(&config.media_books_root)?;
    Ok(shelf_path_id_map)
}

/// Scans all books from disk into database. Returns Vec of all paths scanned.
fn scan_all_books(
    discoverer: &MediaDiscoverer,
    database: &mut MediaDatabase,
    id_gen: &ResourceId,
    config: &Config
) -> Result<PathIdMap> {
    config.ui_hooks.scan_media_discovery_begin(&config.media_books_root)?;

    let meta_man = MetadataManager::new(&config);
    let mut book_path_id_map = PathIdMap::new();

    // Create book information
    let mut seen_media_ids = HashSet::<String>::new();
    for book_path in discoverer.all_books() {
        let book_path = book_path.context(DiscoveryError {})?;
        let book_relative_path = book_path
            .strip_prefix(&config.media_books_root)
            .expect("Book path not in books root");

        // Database method returns an Option<Book>, convert it into a Result
        // containing a tuple with the desired data
        let (book_id, do_scan_book, is_new) = database.get_book_with_path(&book_relative_path)
            .map(|b| {
                let book_modified_date = book_path.metadata()
                    .context( PathRead {path: &book_path} )?
                    .modified()
                    .context( PathRead {path: &book_path} )?;
                let book_modified_date = DateTime::<Utc>::from(book_modified_date);
                let modified = book_modified_date > b.date_modified;
                if modified {
                    config.ui_hooks.scan_media_item_begin(
                        &config.media_books_root,
                        &book_relative_path,
                        UiMediaType::Book
                    )?;
                }
                Ok( (b.id, modified, false) )
            })
            .unwrap_or_else(|| {
                config.ui_hooks.scan_media_item_begin(
                    &config.media_books_root,
                    &book_relative_path,
                    UiMediaType::Book
                )?;
                config.ui_hooks.scan_media_id_gen(&config.media_books_root, &book_relative_path)?;
                id_gen.create_book_id(&book_relative_path)
                    .map(|i| (i, true, true) )
                    .context( ResourceIdError {} )
            })?;

        // Check for duplicate IDs
        let new_id = book_id.clone();
        if seen_media_ids.contains(&new_id) {
            let orig_media = database.get_book(&book_id)
                .expect("Known to be in database because it's a duplicate");
            Err(Error::DuplicateIdError {
                orig_path: orig_media.path,
                dupe_path: book_relative_path.to_path_buf(),
                id: new_id
            })?;
        } else {
            seen_media_ids.insert(new_id);
        }

        if do_scan_book {
            config.ui_hooks.scan_media_metadata(&config.media_books_root, &book_relative_path)?;
            scan_book(&book_relative_path, &book_id, database, &meta_man, config)?;
            let ui_action = if is_new { UiAction::Create } else { UiAction::Update };
            config.ui_hooks.scan_media_item_end(&config.media_books_root, &book_relative_path, &book_id, ui_action)?;
        } else {
            config.ui_hooks.scan_media_item_end(
                &config.media_books_root, &book_relative_path, &book_id, UiAction::Skip
            )?;
        }

        book_path_id_map.insert(book_relative_path.to_owned(), book_id);
    }

    config.ui_hooks.scan_media_discovery_end(&config.media_books_root)?;
    Ok(book_path_id_map)
}

/// Load book metadata
fn scan_book(
    relative_path: &Path,
    book_id: &str,
    database: &mut MediaDatabase,
    meta_man: &MetadataManager,
    config: &Config
) -> Result<()> {
    let full_path = config.media_books_root.join(relative_path);

    let book_url = encode_path_as_url(&relative_path);
    let book_metadata = meta_man.get_book(&full_path, book_id)
        .context(LoadMetadata {path: &full_path, id: book_id})?;

    // Find all book images (will be None if not a bare directory)
    let page_urls = get_book_image_urls(&full_path, &config.media_books_root)?;

    let mut book = Book::new(book_id, book_metadata, book_url, relative_path);
    book.page_urls = page_urls;

    database.add_or_update_book(book).context( DatabaseOperationError )?;

    Ok(())
}

/// Remove outdated entries from database
fn prune_database(
    database: &mut MediaDatabase,
    valid_shelf_paths: &PathIdMap,
    valid_book_paths: &PathIdMap,
    config: &Config
) -> Result<()> {
    let orphan_shelves = database.get_all_shelves()
        .filter(|s| !valid_shelf_paths.contains_key(&s.path) )
        .collect::<Vec<_>>();
    for shelf in orphan_shelves {
        config.ui_hooks.scan_prune_shelf(&config.media_books_root, &shelf)?;
        database.delete_shelf(&shelf.id)
            .context(DatabaseShelfRemoveError {shelf_id: shelf.id})?;
    }

    let orphan_books = database.get_all_books()
        .filter(|b| !valid_book_paths.contains_key(&b.path) )
        .collect::<Vec<_>>();
    for book in orphan_books {
        config.ui_hooks.scan_prune_media(&config.media_books_root, &book)?;
        database.delete_book(&book.id)
            .context(DatabaseRemoveError {media_id: book.id})?;
    }

    Ok(())
}

/// Establish relationships between database entries, e.g. shelf contents,
/// next/prev book IDs
fn link_database_entries(
    database: &mut MediaDatabase,
    shelf_path_id_map: &PathIdMap,
    book_path_id_map: &PathIdMap,
    root_path: &Path
) -> Result<()> {
    // Each entry in this map ties the path of a shelf to a Vec of IDs of the
    // media contained directly within
    let mut shelf_path_content_ids: HashMap::<&Path, Vec<&str>> = shelf_path_id_map.keys()
        .map(|p| (p.as_path(), Vec::<&str>::new()))
        .collect();
    // Similar to above but for shelves
    let mut shelf_path_subshelf_ids = shelf_path_content_ids.clone();
    // Populate shelf path/content mapping
    for (book_path, book_id) in book_path_id_map.iter() {
        let book_shelf_path = book_path.parent()
            .expect("All books should have a parent directory");
        shelf_path_content_ids.get_mut(book_shelf_path)
            .expect("All books should have an associated shelf with the parent path")
            .push(book_id);
    }
    // Populate shelf path/subshelf mapping
    for (shelf_path, shelf_id) in shelf_path_id_map.iter() {
        // Root shelf has a None parent and is skipped
        shelf_path.parent()
            .map(|parent| shelf_path_subshelf_ids.get_mut(parent)
                .expect("All shelf paths should be in ID mapping")
                .push(shelf_id)
            );
    }

    // Mapping of shelf IDs to names to use in sorting
    let shelf_names: HashMap<String, String> = database.get_all_shelves()
        .map(|s| (s.id, s.name.to_uppercase()))
        .collect();

    for (shelf_path, shelf_book_ids) in shelf_path_content_ids.drain() {
        let books_on_shelf = shelf_book_ids.iter()
            .map(|i| {
                let book = database.get_book(i)
                    .expect("Book ID should exist in database");
                (i.to_owned(), book)
            })
            .collect::<HashMap<_, _>>();

        let sorted_book_ids = sort_shelf_content_ids(
            shelf_book_ids,
            &root_path,
            shelf_path,
            &books_on_shelf,
            book_path_id_map
        )?;

        sequence_shelf_media(&sorted_book_ids, &books_on_shelf, database)?;

        let shelf_id = shelf_path_id_map.get(shelf_path)
            .expect("All shelf paths should be in shelf_path_id_map");
        database.set_shelf_contents(&shelf_id, sorted_book_ids)
            .context(DatabaseOperationError)?;

        // Sort the shelf's shelves by name
        let mut subshelf_id_list = shelf_path_subshelf_ids.remove(shelf_path)
            .expect("All shelf entries already populated");
        subshelf_id_list.sort_unstable_by_key(|k|
            shelf_names.get(*k)
                // Unknown shelf IDs shouldn't happen here and are a scanner error
                .expect(&format!("Unknown shelf ID {}", &k))
        );
        database.set_shelf_subshelves(shelf_id, subshelf_id_list)
            .context(DatabaseOperationError)?;
    }

    Ok(())
}

/// Sorts an array of media IDs for a shelf's content based on an explicit
/// ordering or alphabetization
fn sort_shelf_content_ids<'shelf_ids>(
    mut shelf_book_ids: Vec<&'shelf_ids str>,
    root_path: &Path,
    shelf_path: &Path,
    books_on_shelf: &HashMap<&str, Book>,
    book_path_id_map: &PathIdMap
) -> Result<Vec<&'shelf_ids str>> {
    let order_path = root_path.join(shelf_path).join("order.txt");
    let mut sorted_book_ids = Vec::<&str>::new();
    if order_path.exists() {
        let order_reader = File::open(&order_path)
            .context(PathRead {path: &order_path})?;
        let order_reader = BufReader::new(order_reader);
        let mut book_order: Vec<String> = Vec::new();
        for line in order_reader.lines() {
            // Read will fail if any non-UTF8 filenames are found
            let line = line.context(PathRead {path: &order_path})?;
            book_order.push(line);
        }

        for ordered_filename in book_order {
            let ordered_path = shelf_path.join(&ordered_filename);
            // Books in order list that don't exist are ignored
            if let Some(ordered_id) = book_path_id_map.get(&ordered_path) {
                // UNSTABLE: remove_item() https://github.com/rust-lang/rust/issues/40062
                let ordered_position = shelf_book_ids.iter()
                    .position(|i| &i == &ordered_id)
                    .expect("ID from bookmap not in shelf");
                let shelf_book = shelf_book_ids.remove(ordered_position);
                sorted_book_ids.push(shelf_book);
            }
        }
    }
    // Remaining unorded books get default (title) sort
    shelf_book_ids.sort_unstable_by_key(|k|
        books_on_shelf.get(k)
            .expect("Map was generated from IDs and contains all keys")
            .title.to_uppercase()
    );
    sorted_book_ids.append(&mut shelf_book_ids);
    Ok(sorted_book_ids)
}


/// Assigns next/previous IDs in series
fn sequence_shelf_media(
    sorted_media_ids: &Vec<&str>,
    media_on_shelf: &HashMap<&str, Book>,
    database: &mut MediaDatabase
) -> Result<()> {
    // NOTE: Assumes no media IDs appear more than once across shelves. If two
    // shelves both contain the same media next/prev IDs will be overwritten.
    for (book_id_index, book_id) in sorted_media_ids.iter().enumerate() {
        // Last/first books have no next/prev IDs, they will remain None
        let next_id = sorted_media_ids.get(book_id_index + 1)
            .map(|n| String::from(*n) );
        // Check value of index before getting prev_id, -1 not allowed as
        // array index
        let prev_id = book_id_index.checked_sub(1)
            .map(|p| String::from(sorted_media_ids[p]) );
        let mut book = media_on_shelf.get(book_id)
            .expect("Book map contains same IDs as sorted list")
            .to_owned();
        book.next_id = next_id;
        book.prev_id = prev_id;
        database.add_or_update_book(book)
            .context(DatabaseOperationError)?;
    }

    Ok(())
}


/**
Creates URLs for images in a directory. Returns Some(<Vec of URLs as Strings>)
if book is a bare directory, or None if not. Return is wrapped in a Result.

# Errors

May return I/O errors while reading filenames from disk.

# Panics
Will panic if book_path is not below books_root_path
*/
fn get_book_image_urls(book_path: &Path, books_root_path: &Path) -> Result<Option<Vec<String>>> {
    let book_relative_path = book_path
        .strip_prefix(books_root_path)
        .expect("Path is not below root");
    if book_path.is_dir() {
        let mut images = book_path.read_dir()
            .context(DirectoryRead {path: &book_path})?
            .filter_map(|e| {
                // Translate DirEntry into path and read_dir errors into Error,
                // then filter out non-image paths
                let result_with_path = e.context(DirectoryRead {path: &book_path})
                    .map(|e_ok| book_relative_path.join(e_ok.file_name()));
                if let Ok(entry_path) = result_with_path.as_ref() {
                    // UNSTABLE: bool::then
                    match path_is_image(entry_path) {
                        true => Some(result_with_path),
                        false => None
                    }
                } else {
                    None
                }
            })
            .map(|i| i.map(|p| encode_path_as_url(&p) ) )
            .collect::<Result<Vec<String>>>()?;
        images.sort_unstable();
        return Ok(Some(images));
    } else {
        return Ok(None);
    }
}


#[cfg(test)]
#[path = "./scan.test.rs"]
mod scan_test;
