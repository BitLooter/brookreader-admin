use std::{
    fmt::{self, Display},
    fs,
    hash::Hasher,
    io,
    path::{Path, PathBuf},
    str::FromStr,
};
use snafu::{OptionExt, ResultExt, Snafu};

use crate::archive::{open_archive, Error as ArchiveError};
use crate::config::Config;
use super::hash::{create_hasher_xxhash64, hash_xxhash64};


#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Could not read archive {}: {}", path.display(), source))]
    ArchiveReadError { path: PathBuf, source: ArchiveError },
    #[snafu(display("Could not read book file {}: {}", path.display(), source))]
    BookRead { path: PathBuf, source: io::Error },
    #[snafu(display("ID type is invalid or unknown: {}", id_type))]
    InvalidIdTypeError { id_type: String },
    #[snafu(display("Path not valid unicode: {}", path.display()))]
    UnicodeError { path: PathBuf },
}
type Result<T, E = Error> = std::result::Result<T, E>;

enum IdType {
    ContentHash,
    Filename,
    PathHash,
}

impl Display for IdType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            Self::ContentHash => "content",
            Self::Filename => "filename",
            Self::PathHash => "path",
        })
    }
}

impl FromStr for IdType {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "content" => Ok(Self::ContentHash),
            "path" => Ok(Self::PathHash),
            "filename" => Ok(Self::Filename),
            _ => Err(Self::Err::InvalidIdTypeError {id_type: s.to_owned()} )
        }
    }
}

pub struct ResourceId {
    base_path: PathBuf,
    filename_hash_length: usize,
    media_id_type: IdType,
    shelf_id_type: IdType,
}

impl ResourceId {
    /// Create a new ResourceId struct
    pub fn new(config: &Config) -> Result<Self> {
        Ok(Self {
            // TODO: Don't clone, store a reference
            base_path: config.media_books_root.clone(),
            filename_hash_length: config.scan_filename_id_max_length,
            media_id_type: IdType::from_str(&config.scan_id_type_media)?,
            shelf_id_type: IdType::from_str(&config.scan_id_type_shelf)?,
        })
    }

    /// Calculate the ID for the given book path. ID is based on a hash of the
    /// book data. Path given should be relative to base_path.
    pub fn create_book_id(&self, book_path: &Path) -> Result<String> {
        match self.media_id_type {
            IdType::ContentHash => self.generate_content_hash(book_path),
            IdType::Filename => self.strip_filename(book_path),
            IdType::PathHash => self.generate_path_hash(book_path),
        }
    }

    /// Calculate the ID for the given shelf path. ID is based on hash of
    /// relative path. Path given should be relative to base_path.
    pub fn create_shelf_id(&self, shelf_path: &Path) -> Result<String> {
        match self.shelf_id_type {
            IdType::ContentHash => panic!(
                "ID type validity should have been checked when config was loaded. Invalid shelf ID type: {}",
                self.shelf_id_type
            ),
            IdType::Filename => self.strip_filename(shelf_path),
            IdType::PathHash => self.generate_path_hash(shelf_path),
        }
    }

    /// Create an ID hash for a book based on book data
    fn generate_content_hash(&self, content_path: &Path) -> Result<String> {
        // Could check if path is relative and only convert if true if it makes
        // things easier in the future
        let book_path = self.base_path.join(content_path);
        let book_hash: u64;
        if book_path.is_file() {
            let mut book_file = fs::File::open(&book_path)
                .context(BookRead {path: &book_path})?;
            let mut hasher = create_hasher_xxhash64();
            // TODO: Hash based on decoded data? Books would have the same hash whether zip,
            // rar, or dir
            io::copy(&mut book_file, &mut hasher)
                .context(BookRead {path: &book_path})?;
            book_hash = hasher.finish();
        } else if book_path.is_dir() {
            let book_archive = open_archive(&book_path)
                .context(ArchiveReadError {path: &book_path})?;
            let mut hasher = create_hasher_xxhash64();
            let book_images = book_archive.images()
                .context(ArchiveReadError {path: &book_path})?;
            for image_path in book_images {
                let mut image_file = fs::File::open(book_path.join(&image_path))
                    .context(BookRead {path: &image_path})?;
                io::copy(&mut image_file, &mut hasher)
                    .context(BookRead {path: &image_path})?;
            }
            book_hash = hasher.finish();
        } else {
            panic!(format!("Can't get ID of {}, not file or dir", book_path.display()));
        }
        Ok(format!("{:016x}", book_hash))
    }

    /// Generate an ID hash from Path components
    fn generate_path_hash(&self, content_path: &Path) -> Result<String> {
        // Combine path components without path separators for hash
        let hash_source = content_path.components()
            .map(|c| c.as_os_str().to_string_lossy() )
            .collect::<Vec<_>>()
            .join("");
        let hash = hash_xxhash64(hash_source);
        let new_id = format!("{:016x}", hash);
        Ok(new_id)
    }

    /// Create an ID based on modified filename text. Converts whitespace and
    /// periods into underscores, strips out all other punctuation except
    /// dashes, and uses what remains as the ID. Returns error on non-unicode
    /// filenames.
    fn strip_filename(&self, content_path: &Path) -> Result<String> {
        let mut stripped_name = content_path.file_stem()
            .expect("Media/shelf paths should have file name")
            .to_str()
            .context( UnicodeError {path: content_path})?
            .chars()
            .filter_map(|c| {
                if c.is_whitespace() || c == '.' || c == '_' {
                    Some('_')
                } else if c != '-' && c.is_ascii_punctuation() {
                    None
                } else {
                    Some(c)
                }
            })
            .collect::<String>();
        // TODO: Why?
        stripped_name.truncate(self.filename_hash_length);
        Ok(stripped_name)
    }
}


#[cfg(test)]
#[path = "./resourceid.test.rs"]
mod resourceid_test;
