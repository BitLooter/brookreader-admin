use clap::ArgMatches;

use crate::tests_common;


#[test]
fn read_configuration_file() {
    let config_path = tests_common::testenv_root().join("Brookreader.toml");
    let args = ArgMatches::new();

    let config = super::Config::load(&config_path, &args)
        .expect(&format!("Could not load config file {:?}", config_path));

    // Default setting
    assert!(config.brookreader_data_path.ends_with("data"));
    // Overridden in config
    assert!(config.media_books_root.ends_with("static/books"));
    assert_eq!("books", config.media_books_url);
    assert_eq!("filename", config.scan_id_type_media);
    assert_eq!("filename", config.scan_id_type_shelf);
}

// TODO: validate()
