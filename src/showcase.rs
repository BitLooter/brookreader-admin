use std::{
    collections::HashSet,
    fs::{self, File},
    io::{self, BufRead, BufReader},
    path::{Path, PathBuf}
};


/// Represents the items in a given showcase index, e.g. featured items. Items
/// are referenced by ID.
#[derive(Debug)]
pub struct ShowcaseList {
    pub items: HashSet<String>,
    pub shelves: HashSet<String>,
    index_path: PathBuf
}

impl ShowcaseList {
    /// Add a new media ID. Does nothing if already present.
    pub fn add_media(&mut self, media_id: &str) {
        self.items.insert(media_id.to_owned());
    }

    /// Add a new shelf ID. Does nothing if already present.
    pub fn add_shelf(&mut self, shelf_id: &str) {
        self.shelves.insert(shelf_id.to_owned());
    }

    /// Load new showcase from index at given path
    pub fn load(index_path: &Path) -> Result<Self, io::Error> {
        let mut shelves: HashSet<String> = HashSet::new();
        let mut items: HashSet<String> = HashSet::new();

        let showcase_reader = BufReader::new(File::open(index_path)?);
        let mut is_book_id = false;
        for entry_id in showcase_reader.lines() {
            let entry_id = entry_id?;

            // Blank lines are ignored
            if entry_id.is_empty() {
                continue;
            }

            // A line that begins with * divides shelf IDs from media IDs
            if entry_id.starts_with("*") {
                is_book_id = true;
                continue;
            }

            if is_book_id {
                items.insert(entry_id);
            } else {
                shelves.insert(entry_id);
            }
        }

        Ok(Self {
            items,
            shelves,
            index_path: index_path.to_path_buf()
        })
    }

    // Save showcase to disk. Format is list of IDs, one per line, with
    // "*BOOKS" seperating shelf IDs from media IDs.
    pub fn save(&self) -> Result<(), io::Error> {
        let mut output = String::new();
        for shelf in self.shelves.iter() {
            output.push_str(shelf);
            output.push_str("\n");
        }
        output.push_str("*BOOKS\n");
        for item in self.items.iter() {
            output.push_str(item);
            output.push_str("\n");
        }
        fs::write(&self.index_path, output)?;
        Ok(())
    }
}


#[cfg(test)]
#[path = "./showcase.test.rs"]
mod showcase_test;
