use std::{
    cell::RefCell,
    fmt,
    fs::File,
    io::{self, Write},
    path::Path
};
use console::{self, Style, Term};
use chrono::{Datelike, Local, Utc};
use indicatif::{ProgressBar, ProgressStyle};

use crate::mediadb::{Book, Shelf};
use crate::build::stats::Stats;
use crate::ui::{self, UiAction, UiMediaType, UserInterface};


// Last character not rendered by indicatif for some reason
const SPINNER_CHARS: &str = "⣾⣽⣻⢿⡿⣟⣯⣷⣾";
const SPINNER_CHARS_0214: &str = "💘💗💖💔💘";
const SPINNER_CHARS_0401: &str = "🙈🙉🙊🙈";
const SPINNER_CHARS_0422: &str = "🌎🌏🌍🌎";
const SPINNER_CHARS_1225: &str = "🌲🎄🌲";

const SPINNER_TEMPLATE: &str = "{spinner} {wide_msg:.dim}";

// TODO: Should probably rename this, does a lot more than logging now

pub struct Logger {
    log_file: RefCell<Box<dyn Write>>,
    spinner: RefCell<Option<ProgressBar>>,
    spinner_progress_style: ProgressStyle,
    spinner_text_style: Style,
    terminal: Term,
    /// Style for text displayed only in verbose mode
    verbose_style: Style,
    warn_style: Style,
    warn_label_style: Style,
    warnings: RefCell<Vec<String>>,
}

impl Logger {
    /// Create a new instance using the given writer for log output
    pub fn new(mut log_file: impl Write + 'static) -> Self {
        let now = Utc::now();
        let initial_message = format!("Brookreader v{} log file\nOperation started {}\n\n",
            env!("CARGO_PKG_VERSION").to_owned(),
            now.to_rfc3339()
        );
        log_file.write_all(initial_message.as_bytes())
            .expect("Can't write to log file");
        let log_box = Box::new(log_file);
        let terminal = Term::stdout();
        terminal.set_title(env!("CARGO_PKG_NAME"));
        Logger {
            // error_style: Style::new().red(),
            // error_label_style: Style::new().on_red(),
            log_file: RefCell::new(log_box),
            spinner: RefCell::new(None),
            spinner_progress_style: ProgressStyle::default_spinner()
                .template(SPINNER_TEMPLATE)
                .tick_chars(Self::spicy_spinner_chars()),
            // Can't set italic in spinner template string, use this style instead
            spinner_text_style: Style::new().italic(),
            terminal,
            verbose_style: Style::new().dim(),
            warn_style: Style::new().yellow(),
            warn_label_style: Style::new().on_yellow(),
            warnings: RefCell::new(Vec::new()),
        }
    }

    /// Creates a new instance that logs to the given file path
    pub fn new_with_path(log_path: &Path) -> Self {
        let log_file = File::create(&log_path)
            .expect(&format!("Can't open log file {}", log_path.to_string_lossy()));
        Self::new(log_file)
    }

    /// Selects a special spinner depending on date, otherwise normal
    fn spicy_spinner_chars() -> &'static str {
        let current_date = Local::now().date();
        if current_date.month() == 2 && current_date.day() == 14 {
            // Valentines
            SPINNER_CHARS_0214
        } else if current_date.month() == 4 && current_date.day() == 1 {
            // April Fool's
            SPINNER_CHARS_0401
        } else if current_date.month() == 4 && current_date.day() == 22 {
            // Earth day (really just wanted an excuse to use the globe)
            SPINNER_CHARS_0422
        } else if current_date.month() == 12 && current_date.day() == 25 {
            // Christmas
            SPINNER_CHARS_1225
        } else {
            SPINNER_CHARS
        }
    }

    /// Initialize a new progress spinner
    fn make_spinner(&self) {
        self.remove_spinner();
        let spinner = ProgressBar::new_spinner()
            .with_style(self.spinner_progress_style.clone());
        spinner.enable_steady_tick(250);
        self.spinner.replace(Some(spinner));
    }

    /// Set the message displayed on the current progress spinner
    fn set_spinner_message(&self, message: &str) {
        self.spinner.borrow()
            .as_ref()
            .map(|s| s.set_message(
                &self.spinner_text_style.apply_to(message).to_string()
            ));
    }

    /// Clear the existing progress spinner
    fn remove_spinner(&self) {
        self.spinner.replace(None)
            .map(|s| s.finish_and_clear());
    }

    /// Similar to println! but accounts for presence of spinner
    fn println(&self, message: &str) {
        if let Some(spinner) = self.spinner.borrow().as_ref() {
            spinner.println(message);
        } else {
            println!("{}", &message);
        }
    }

    /// Sets title for terminal in the format <program name> - <subtitle>
    fn set_terminal_subtitle(&self, subtitle: &str) {
        let title = format!("{} - {}", env!("CARGO_PKG_NAME"), subtitle);
        self.terminal.set_title(&title);
    }

    // Internal logging methods
    //-------------------------

    fn debug(&self, message: &str) {
        let fancy_message = format!("{}",
            self.verbose_style.apply_to(&message));
        self.println(&fancy_message);
    }

    /// Normal informational message
    fn info(&self, message: &str) {
        let log_message = format!("{}", &message);
        self.println(&log_message);
    }

    /// Minor (non-fatal) error
    fn warn(&self, message: &str) {
        let fancy_message = format!("{}: {}",
            self.warn_label_style.apply_to("Warning"),
            self.warn_style.apply_to(&message));
        self.println(&fancy_message);
        let mut warnings = self.warnings.borrow_mut();
        warnings.push(fancy_message);
    }

    /// Similar to info() but also outputs message to log
    fn info_log(&self, message: &str) -> ui::Result<()> {
        self.info(message);
        self.log(message)
    }

    /// Write a message to the log file
    fn log(&self, message: &str) -> ui::Result<()> {
        writeln!(self.log_file.borrow_mut(), "{}", message)
    }
}

impl UserInterface for Logger {
    fn build_data_begin(&self) -> ui::Result<()> {
        self.set_terminal_subtitle("Building site data");
        Ok(())
    }

    fn build_data_cover_media(&self, book: &Book) -> ui::Result<()> {
        let message = format!(
            "Extracting cover for {}",
            &book.path.file_name()
                .expect("Book has no filename")
                .to_string_lossy()
        );
        self.set_spinner_message(&message);
        Ok(())
    }

    fn build_data_cover_media_error(&self, media: &Book, message: &str) -> ui::Result<()> {
        let message = format!("Error extracting cover for media {}: {}", &media.id, message);
        self.warn(&message);
        Ok(())
    }

    fn build_data_cover_shelf(&self, shelf: &Shelf) -> ui::Result<()> {
        let message = format!(
            "Extracting cover for shelf {}",
            &shelf.path.file_name()
                .expect("Shelf has no filename")
                .to_string_lossy()
        );
        self.set_spinner_message(&message);
        Ok(())
    }

    fn build_data_cover_shelf_error(&self, shelf: &Shelf, message: &str) -> ui::Result<()> {
        let message = format!("Error extracting cover for shelf {}: {}", &shelf.id, message);
        self.warn(&message);
        Ok(())
    }

    fn build_data_covers_begin(&self) -> ui::Result<()> {
        self.info("Generating covers...");
        self.make_spinner();
        Ok(())
    }

    fn build_data_covers_end(&self) -> ui::Result<()> {
        self.remove_spinner();
        Ok(())
    }

    fn build_data_stats(&self, stats: &Stats) -> ui::Result<()> {
        self.info(&format!(
            "Found {} ({}) books",
            stats.library.books_total,
            indicatif::HumanBytes(stats.library.books_size)
        ));
        Ok(())
    }

    fn build_fe_begin(&self) -> ui::Result<()> {
        self.set_terminal_subtitle("Building frontend");
        Ok(())
    }

    fn init_begin(&self, init_path: &Path, config_path: &Path) -> ui::Result<()> {
        self.set_terminal_subtitle("Initializing new site");
        self.info_log(&format!("Initializing new Brookreader site at {}", init_path.display()))?;
        self.info_log(&format!("Writing defaults to {}", config_path.display()))
    }

    fn operation_end(&self) -> ui::Result<()> {
        let warnings_count = self.warnings.borrow().len();
        if warnings_count > 0 {
            self.println("");
            self.warn(&format!("{} warnings were raised, check logs", warnings_count))
        }
        Ok(())
    }

    fn scan_begin(&self, scan_path: &Path) -> ui::Result<()> {
        self.set_terminal_subtitle("Scanning media");
        self.info_log(&format!("Starting scan of {}", scan_path.display()))
    }

    fn scan_media_discovery_begin(&self, _media_dir: &Path) -> ui::Result<()> {
        self.info("Scanning books...");
        Ok(())
    }

    fn scan_media_id_gen(&self, _scan_path: &Path, media_path: &Path) -> ui::Result<()> {
        let message = format!(
            "Generating ID for {}",
            &media_path.file_name()
                .expect("Book has no filename")
                .to_string_lossy() );
        self.set_spinner_message(&message);
        Ok(())
    }

    fn scan_media_item_begin(&self, _scan_path: &Path, _media_path: &Path, _media_type: UiMediaType) -> ui::Result<()> {
        self.make_spinner();
        Ok(())
    }

    fn scan_media_item_end(&self, _scan_path: &Path, media_path: &Path, media_id: &str, scan_action: UiAction) -> ui::Result<()> {
        self.remove_spinner();
        let action = match scan_action {
            UiAction::Create => "Found",
            UiAction::Skip => "Skipping",
            UiAction::Update => "Updated",
        };
        let media_filename = media_path.file_name()
            .expect("Book has no filename")
            .to_string_lossy();
        let message = format!("{}: {} (ID: {})", action, media_filename, media_id);
        match scan_action {
            UiAction::Skip => self.debug(&message),
            _ => self.info_log(&message)?
        }
        Ok(())
    }

    fn scan_media_metadata(&self, _scan_path: &Path, book_path: &Path) -> ui::Result<()> {
        let message = format!(
            "Gathering metadata for {}",
            &book_path.file_name()
                .expect("Book has no filename")
                .to_string_lossy() );
        self.set_spinner_message(&message);
        Ok(())
    }

    fn scan_prune_media(&self, _scan_path: &Path, book: &Book) -> ui::Result<()> {
        self.debug(
            &format!("Removed nonexistent book {} ({}) from database",
                &book.path.display(),
                &book.id
            )
        );
        Ok(())
    }

    fn scan_prune_shelf(&self, _scan_path: &Path, shelf: &Shelf) -> ui::Result<()> {
        self.debug(
            &format!("Removed nonexistent shelf {} ({}) from database",
                &shelf.path.display(),
                &shelf.id
            )
        );
        Ok(())
    }

    fn showcase_add_media(&self, showcase_name: &str, media: &Book) -> ui::Result<()> {
        self.info_log(&format!(
            r#"Added media "{}" (ID: {}) to {} showcase"#,
            &media.path.display(),
            &media.id,
            showcase_name
        ))
    }

    fn showcase_add_shelf(&self, showcase_name: &str, shelf: &Shelf) -> ui::Result<()> {
        self.info_log(&format!(
            r#"Added shelf "{}" (ID: {}) to {} showcase"#,
            &shelf.path.display(),
            &shelf.id,
            showcase_name
        ))
    }

    fn showcase_begin(&self, showcase_name: &str) -> ui::Result<()> {
        self.set_terminal_subtitle("Modifying showcase");
        self.info_log(&format!("Editing showcase {}", showcase_name))
    }
}

impl Default for Logger {
    fn default() -> Logger {
        Self::new(io::sink())
    }
}

impl fmt::Debug for Logger {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Logger")
    }
}
