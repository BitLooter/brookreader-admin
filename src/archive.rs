use std::{
    cell::RefCell,
    collections::HashMap,
    io::{self, ErrorKind},
    ffi::OsStr,
    fs,
    path::{Path, PathBuf},
    process::Command,
    vec::Vec,
};
use std::io::{Read};    // Needed for ZipArchive
use snafu::{OptionExt, ResultExt, Snafu};
use zip::ZipArchive;

use crate::util::{path_is_image, path_is_rar, path_is_zip};


thread_local! {
    // Path needs to be set with set_unrar_path() before rar archives are read
    static UNRAR_PATH: RefCell<PathBuf> = RefCell::new(PathBuf::new());
}

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Could not read contents of directory '{}': {}", path.display(), source))]
    DirRead { path: PathBuf, source: io::Error },
    #[snafu(display("Missing unrar executable"))]
    ExecutableMissing {},
    #[snafu(display("Could not run unrar: {}", source))]
    ExecutableInvalid { source: io::Error },
    #[snafu(display("Could not read file '{}': {}", path.display(), source))]
    FileRead { path: PathBuf, source: io::Error },
    #[snafu(display("Path '{}' is not a known archive type", path.display() ))]
    UnknownArchive { path: PathBuf },
    #[snafu(display("No file '{}' in archive '{}'", file.display(), archive.display() ))]
    UnknownFilename { file: PathBuf, archive: PathBuf },
    #[snafu(display("Could not extract file '{}' from archive '{}': {}",
                    file.display(), path.display(), source))]
    ZipExtract { file: PathBuf, path: PathBuf, source: zip::result::ZipError },
    #[snafu(display("Could not read zip archive '{}': {}", path.display(), source))]
    ZipRead { path: PathBuf, source: zip::result::ZipError }
}
type Result<T, E = Error> = std::result::Result<T, E>;

/* Utility functions
 ********************/
// Autodetects archive type and opens it with appropriate reader
pub fn open_archive(path: &Path) -> Result<Box<dyn ArchiveReader>> {
    let reader: Box<dyn ArchiveReader>;
    if path.is_dir() {
        reader = Box::new(ArchiveReaderDir::new(path)?);
    } else if path_is_rar(path) {
        reader = Box::new(ArchiveReaderRar::new(path)?);
    } else if path_is_zip(path) {
        reader = Box::new(ArchiveReaderZip::new(path)?);
    } else {
        return Err(Error::UnknownArchive {path: path.to_owned()})
    }
    Ok(reader)
}

/// Sets the path of the executable for unrar
pub fn set_unrar_path<P>(path: P)
where P: Into<PathBuf>
{
    UNRAR_PATH.with(|p| {
        *p.borrow_mut() = path.into();
    });
}

/* Common code
 **************/
pub trait ArchiveReader {
    /// Return all file paths in archive
    fn files(&self) -> Result<Vec<PathBuf>>;
    /// Path the reader represents
    fn path(&self) -> &Path;
    // TODO: file_path should be as_ref()
    /// Read contents of file in archive
    fn read_file_data(&self, file_path: &Path) -> Result<Vec<u8>>;

    /// Returns contained files that are images
    fn images(&self) -> Result<Vec<PathBuf>> {
        let mut image_paths = self.files()?;
        image_paths.retain(|p| path_is_image(p));
        Ok(image_paths)
    }

}

/// ArchiveReader for bare directories
pub struct ArchiveReaderDir {
    dir_path: PathBuf
}

impl ArchiveReaderDir {
    pub fn new(path: &Path) -> Result<Self> {
        Ok(Self {
            dir_path: path.to_owned()
        })
    }
}

impl ArchiveReader for ArchiveReaderDir {
    fn read_file_data(&self, file_path: &Path) -> Result<Vec<u8>> {
        let file_data = fs::read(self.dir_path.join(file_path))
            .context(FileRead {path: file_path})?;
        Ok(file_data)
    }

    fn files(&self) -> Result<Vec<PathBuf>> {
        let mut file_paths: Vec<PathBuf> = Vec::new();
        let dir_entries = self.dir_path.read_dir()
            .context(DirRead {path: &self.dir_path})?;
        for entry in dir_entries {
            let entry = entry.context(DirRead {path: &self.dir_path})?
                .path()
                .strip_prefix(&self.dir_path)
                .expect("Removing common prefix from archive dir")
                .to_owned();
            file_paths.push(entry);
        }
        Ok(file_paths)
    }

    fn path(&self) -> &Path {
        &self.dir_path
    }
}

/// ArchiveReader for ZIP/CBZ/EPUB/etc. files
pub struct ArchiveReaderZip {
    archive_path: PathBuf,
    path_map: HashMap<PathBuf, String>,
    zip: RefCell<ZipArchive<fs::File>>
}

impl ArchiveReaderZip {
    fn new(path: &Path) -> Result<Self> {
        let file = fs::File::open(path)
            .context(FileRead {path})?;
        let mut zip = ZipArchive::new(file)
            .context(ZipRead {path})?;
        // Zip uses &str as a file identifier, path_map is used to convert
        // santized paths given to calling code back into their original filenames
        let mut path_map: HashMap<PathBuf, String> = HashMap::new();
        for entry_index in 0..zip.len() {
            // NOTE: ZipArchive::file_names() may be more efficient here but
            // testing needs to be done to compare its handling of non-unicode
            // filenames to how the current code does. Would also require
            // rewriting all code to use &str instead of &Path to refer to file
            // entries.
            // TODO: Maybe &str actually would make more sense as a page identifier? PathBuf
            // is used because the earliest versions of the code only supported raw directories,
            // but now that it mostly reads from archives that's less reasonable
            let zip_entry = zip.by_index(entry_index)
                .context(ZipRead {path})?;
            path_map.insert(zip_entry.name().into(), zip_entry.name().to_owned());
        }
        Ok(Self {
            archive_path: path.to_owned(),
            path_map,
            zip: RefCell::new(zip)
        })
    }
}

impl ArchiveReader for ArchiveReaderZip {
    fn read_file_data(&self, file_path: &Path) -> Result<Vec<u8>> {
        // ZipArchive must be mutable even if only reading
        let mut zip = self.zip.borrow_mut();
        let file_path_str = self.path_map.get(file_path)
            .context(UnknownFilename {file: file_path.to_owned(), archive: self.path()})?;
        let mut zip_entry = zip.by_name(&file_path_str)
            .context(ZipExtract {file: file_path.to_owned(), path: self.path()})?;
        let mut zip_entry_data: Vec<u8> = Vec::new();
        zip_entry.read_to_end(&mut zip_entry_data)
            .context(FileRead {path: self.path()})?;
        Ok(zip_entry_data)
    }

    fn files(&self) -> Result<Vec<PathBuf>> {
        let file_paths: Vec<_> = self.path_map.keys()
            .map(|e| e.to_path_buf())
            .collect();

        Ok(file_paths)
    }

    fn path(&self) -> &Path {
        &self.archive_path
    }
}

/// ArchiveReader for RAR/CBR files
///
/// Note that this reader depends on the unrar program being installed
pub struct ArchiveReaderRar {
    archive_path: PathBuf,
    path_map: HashMap<PathBuf, String>,
    unrar_executable: PathBuf
}

impl ArchiveReaderRar {
    fn new(path: &Path) -> Result<Self> {
        let book_path_str = path.as_os_str();
        let unrar_executable: PathBuf = UNRAR_PATH.with(|p| {
            p.replace_with(|v| v.clone())
        });

        let output = Command::new(&unrar_executable)
            .args(&[OsStr::new("lb"), book_path_str])
            .output()
            .map_err(Self::command_error_mapper)?;

        let mut path_map: HashMap<PathBuf, String> = HashMap::new();
        // At this time thanks to its reliance on an external executable, it is
        // not possible to open rar files with non-unicode filenames. As it is
        // unlikely that a CBR file will use anything but simple ascii
        // filenames this is a low-priority issue that would likely be solved
        // anyways once a more integrated solution is developed. If you have
        // any rar files that cannot be read I suggest as a solution for now
        // that you fix your broken rar files instead.
        for entry in String::from(std::str::from_utf8(&output.stdout).unwrap()).lines() {
            let file_path = PathBuf::from(entry);
            path_map.insert(file_path, entry.to_owned());
        }

        Ok(Self {
            archive_path: path.to_owned(),
            path_map,
            unrar_executable
        })
    }

    fn command_error_mapper(error: io::Error) -> Error {
        if error.kind() == ErrorKind::NotFound {
            return Error::ExecutableMissing {}
        } else {
            return Error::ExecutableInvalid { source: error }
        }
    }
}

impl ArchiveReader for ArchiveReaderRar {
    fn read_file_data(&self, file_path: &Path) -> Result<Vec<u8>> {
        let archive_path = self.archive_path.as_os_str();
        let file_path = file_path.as_os_str();
        let rar_output = Command::new(&self.unrar_executable)
            .args(&[OsStr::new("p"), OsStr::new("-inul"), archive_path, file_path])
            .output()
            .map_err(Self::command_error_mapper)?;
        Ok(rar_output.stdout)
    }

    fn files(&self) -> Result<Vec<PathBuf>> {
        let file_paths: Vec<_> = self.path_map.keys()
            .map(|e| e.to_path_buf())
            .collect();

        Ok(file_paths)
    }

    fn path(&self) -> &Path {
        &self.archive_path
    }
}


#[cfg(test)]
#[path = "./archive.test.rs"]
mod archive_test;
