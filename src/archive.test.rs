use std::path::PathBuf;

use crate::tests_common;


#[test]
fn read_zip_archive() {
    let zip_path = tests_common::testenv_root().join("static/books/book_formats/test-cbz.cbz");
    let archive = super::open_archive(&zip_path)
        .expect("Could not open zip archive");
    let mut archive_filenames = archive.files()
        .expect("Could not get archive contents");
    archive_filenames.sort_unstable();

    assert_eq!(
        vec![PathBuf::from("1.png"), PathBuf::from("2.png"), PathBuf::from("3.png")],
        archive_filenames
    );

    let entry_data = archive.read_file_data(std::path::Path::new("1.png"))
        .expect(&format!("Could not read entry 1.png from {:?}", &zip_path));
    assert_eq!(3903, entry_data.len());
}

#[test]
fn rar_reading_fails_until_path_set() {
    let rar_path = tests_common::testenv_root().join("static/books/book_formats/test-cbr.cbr");
    let failed_open_result = super::open_archive(&rar_path);
    assert!(failed_open_result.is_err());

    super::set_unrar_path("/usr/bin/unrar");
    let successful_open_result = super::open_archive(&rar_path);
    assert!(successful_open_result.is_ok());
}

#[test]
fn read_rar_archive() {
    super::set_unrar_path("/usr/bin/unrar");

    let rar_path = tests_common::testenv_root().join("static/books/book_formats/test-cbr.cbr");
    let archive = super::open_archive(&rar_path)
        .expect("Could not open rar archive");
    let mut archive_filenames = archive.files()
        .expect("Could not get archive contents");
    archive_filenames.sort_unstable();

    assert_eq!(
        vec![PathBuf::from("1.png"), PathBuf::from("2.png"), PathBuf::from("3.png")],
        archive_filenames
    );

    let entry_data = archive.read_file_data(std::path::Path::new("1.png"))
        .expect(&format!("Could not read entry 1.png from {:?}", &rar_path));
    assert_eq!(3903, entry_data.len());
}

// TODO: Invalid archives
